
import { AppRegistry } from 'react-native';
import Routing from './app/routes/Routing';


global.___DEV___ = false;
console.disableYellowBox = true;

// import App from './App';

AppRegistry.registerComponent('reactTutorialApp', () => Routing );
