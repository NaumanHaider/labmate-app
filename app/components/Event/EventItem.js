import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';

import Config from './../../Services/Config';

import * as Animatable from 'react-native-animatable';

// Assets
import StyleGridList from './../../styles/GridList';

export default class EventItem extends React.Component
{
    constructor( props )
    {
        super(props);
    }

    render()
    {
        let item_event      = this.props.event_item;
        let item_event_last = (item_event[1]);
        
        return (
            <View style={ StyleGridList.boxRow }>

            <TouchableOpacity style={{ flex:1 }}
                    onPress={ () => this.props.navigate('EventDetail', { event_id: item_event[0].id}) }
                >
                <Animatable.View animation="flipInX" style={ StyleGridList.item }>
                    <Image 
                        source={{uri: Config.base_url() + item_event[0].image_chunks.image_url }}
                        style={{ height: 155, borderRadius: 5 }} 
                    />
                    <View style={{backgroundColor:'red', width:"100%", height:40, opacity: 0.4, position:'absolute', borderRadius: 5, top:115}}/>
                    <Text style={ StyleGridList.itemText }>{ item_event[0].title }</Text>
                </Animatable.View>
            </TouchableOpacity>

            {
            (( item_event_last )
            ?
            (<TouchableOpacity style={{ flex:1 }}
                onPress={ () => this.props.navigate('EventDetail', { event_id: item_event[1].id}) }
            >
                <Animatable.View animation="flipInX" style={ StyleGridList.item }>
                    <Image 
                        source={{uri: Config.base_url() + item_event[1].image_chunks.image_url }}
                        style={{ height: 155, borderRadius: 5 }} 
                    />
                    <View style={{backgroundColor:'red', width:"100%", height:40, opacity: 0.4, position:'absolute', borderRadius: 5, top:115}}/>
                    <Text style={ StyleGridList.itemText }>{ item_event[1].title }</Text>
                </Animatable.View>
            </TouchableOpacity>)
            :
            null
            )
            }
        </View>
        );
    }
}

const styles = StyleSheet.create({

	bodyContainer:{
		flexDirection:'row',
		margin:5,
		justifyContent:'center',
	},

    itemText:{
        marginHorizontal:20,
        fontSize:5,
    }
});