import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Checkbox from 'react-native-custom-checkbox';

// Assets
import styles from './../../styles/QuickQuotos';

export default class SelectionProductItem extends React.Component
{
    constructor( props )
    {
        super(props);   
    }

    render()
    {
        let row = this.props.row;
        let row_id = row.id;
        let flatListIndex = this.props.index; 

        return (
            <View style={{ backgroundColor:'#fff', width:270, marginBottom:10, borderRadius:10, borderColor:'#9e9e9ecc', borderWidth:1 }}>
                
                <View style={{flexDirection: 'row'}}> 
                    <View style={ { width:270 } }>
                        <Text style={{ paddingTop:12, paddingBottom:5, paddingHorizontal:20, fontFamily: 'Roboto-Bold', fontWeight: '500', fontSize:18, color:'black' }}>
                            { row.title }
                        </Text>
                    </View>
                    <View style={styles.checkBox}>
                        <TouchableOpacity onPress={ this.props.handleOfDelProduct.bind(this, row_id, flatListIndex) }>
                            <Icon name="times-circle" size={30} color="red" />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{flexDirection: 'row'}}> 
                    <View >
                        <Text style={{ paddingLeft:20, fontFamily: 'Roboto-Bold', fontWeight: '500', fontSize:14, color:'black' }}>Alias: </Text>
                    </View>
                    <View>
                        <Text style={{ paddingLeft:5, fontFamily: 'Roboto-Bold', color:'#9e9e9ecc', fontSize:14 }}>{ row.alias }</Text>
                    </View>
                </View>

                <View style={{flexDirection: 'row'}}> 
                    <View >
                        <Text style={{ marginBottom:15, paddingLeft:20, fontFamily: 'Roboto-Bold', fontWeight: '500', fontSize:14, color:'black' }}>Reference: </Text>
                    </View>
                    <View>
                        <Text style={{ paddingLeft:5, fontFamily: 'Roboto-Bold', color:'#9e9e9ecc', fontSize:14, marginBottom:15 }}>{ row.reference }</Text>
                    </View>
                </View>
            </View>
        );
    }
}