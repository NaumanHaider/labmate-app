import React from 'react';
import { Text, View, Image, ImageBackground, TextInput, TouchableOpacity, Modal, ScrollView, AsyncStorage, KeyboardAvoidingView} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';

import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';

import DeviceInfo from 'react-native-device-info';
import {Select, Option} from "react-native-chooser";
import Spinner from 'react-native-loading-spinner-overlay';

import Config from './../Services/Config';
import Validation from './../Services/Validation';

// Styles & Formatting
import styles from './../styles/UserProfile';
import customStyles from './../styles/Custom';

// Components
import FooterCopyRight from './../components/Settings/FooterCopyRight';

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    mediaType: 'photo',
    path: 'images'
  }
};

export default class ProfileEdit extends React.Component
{
	constructor()
	{
		super();

		this.state = {
			user_id : 0,
			profile: null,

			// Validations
			validate_of_first_name: true,
			validate_of_last_name: true,
			validate_of_username: true,
			validate_of_email: true,
			validate_of_mobile: true,
			validate_of_date_of_birth: true,
			validate_of_gender: true,
			validate_of_address: true,
			validate_of_city: true,
			validate_of_qualification: true,
			validate_of_profession: true,
			validate_of_organization: true,
			validate_of_designation: true,
			validate_of_department: true,
			
			// Edit Form
			edit_of_first_name: false,
			edit_of_username: false,
			edit_of_email: false,
			edit_of_mobile: false,
			edit_of_date_of_birth: false,
			edit_of_gender: false,
			edit_of_address: false,
			edit_of_city: false,
			edit_of_qualification: false,
			edit_of_profession: false,
			edit_of_organization: false,
			edit_of_designation: false,
			edit_of_department: false,
			
			// Set of data
			first_name: '',
			last_name: '',
			username: '',
			email: '',
			mobile: '',
			date_of_birth: '',
			address: '',
			gender: 'Male',

			city: '',
			qualification: '',
			profession: '',
			organization: '',
			designation: '',
			department: '',
			avatar: '',
			avatar_url: '',

			//Loading
			isLoading: false,

			is_saved: false,

			//Modals
			isMessageModal: false,
			isMessageModalText: '',
		}

		AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));
	}

	componentDidMount()
	{
		AsyncStorage.getItem('user_id').then((value) => this.getUserProfile( value  ));
	};

	hanldeOfUploadYourQuery = () => {

	    ImagePicker.showImagePicker(options, (response) => {
	    // console.warn('Response = ', response);

	      if (response.didCancel) {
	        // console.warn('User cancelled image picker');
	      }
	      else if (response.error) {

	        thi.setState({
	          isMessageModal: true,
	          isMessageModalText: response.error
	        });
	        // console.warn('ImagePicker Error: ', response.error);
	      }
	      else 
	      {
	        let file_source = {
	          file_name: response.fileName,
	          file_url: response.uri,
	          source: response.data
	        }

	        this.setState({ avatar: file_source.source, avatar_url: file_source.file_url, is_saved: true });

	        // You can also display the image using data:
	        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
	      }
	    });
	}
	
	_form_validation()
	{
		const _scrollView = this.scrollView;

		let first_name 		= this.state.first_name;
		let last_name 		= this.state.last_name;
		let username 		= this.state.username;
		let email 			= this.state.email;
		let mobile 			= this.state.mobile;
		let date_of_birth 	= this.state.date_of_birth;
		let address 		= this.state.address;
		let city 			= this.state.city;
		let qualification 	= this.state.qualification;
		let profession 		= this.state.profession;
		let organization 	= this.state.organization;
		let designation 	= this.state.designation;
		let department 		= this.state.department;
		let result 			= true;

		// First Name
		if( first_name != "" && Validation.isOnlyString(first_name) )
		{
			this.setState({ validate_of_first_name: true });
		}
		else
		{
			this.setState({ validate_of_first_name: false, edit_of_fullname: true, is_saved: true });
			result = false;
		}

		// Last Name
		if( last_name != "" && Validation.isOnlyString(last_name) )
		{
			this.setState({ validate_of_last_name: true });
		}
		else
		{
			this.setState({ validate_of_last_name: false, edit_of_fullname: true, is_saved: true });
			result = false;
		}
		
		// Phone
		if( mobile != "" && Validation.isNumber(mobile) )
		{
			this.setState({ validate_of_mobile: true });
		}
		else
		{
			this.setState({ validate_of_mobile: false, edit_of_mobile: true, is_saved: true });
			result = false;
		}

		this.setState({ validate_of_date_of_birth: false });

		// Date of Birth
		if( date_of_birth != "" )
		{
			let split_date_of_birth 	= date_of_birth.split('-');
			let user_date_of_birth 		= new Date( split_date_of_birth[2] + '-' + split_date_of_birth[1] + '-' + split_date_of_birth[0] ).getFullYear();
			let user_current_of_birth 	= new Date().getFullYear();
			let diff_of_age 			= ( ((user_current_of_birth - user_date_of_birth) < 18) ? true : false );
			
			// console.warn( 'date_of_birth: ' + date_of_birth);
			// console.warn( 'user_date_of_birth: ' + user_date_of_birth);
			// console.warn( 'diff_of_age: ' + diff_of_age);

			if( diff_of_age )
			{
				this.setState({ validate_of_date_of_birth: false, edit_of_date_of_birth: true, is_saved: true });
				result = false;
			}
			else
			{
				this.setState({ validate_of_date_of_birth: true });
			}
		}
		else
		{
			this.setState({ validate_of_date_of_birth: false, edit_of_date_of_birth: true, is_saved: true });

			result = false;
		}

		/*if( date_of_birth != "" )
		{
			this.setState({ validate_of_date_of_birth: true });
		}
		else
		{
			this.setState({ validate_of_date_of_birth: false, edit_of_date_of_birth: true, is_saved: true });
			result = false;
		}*/

		// Address
		if( address != "" && Validation.isNotAllowSpecialCharacters(address) )
		{
			this.setState({ validate_of_address: false, edit_of_address: true, is_saved: true });
			result = false;
		}
		else
		{
			this.setState({ validate_of_address: true });
		}

		// Qualification
		if( qualification != "" && Validation.isNotAllowSpecialCharacters(qualification) )
		{
			this.setState({ validate_of_qualification: false, edit_of_qualification: true, is_saved: true });
			result = false;
		}
		else
		{
			this.setState({ validate_of_qualification: true });
		}

		// Profession
		if( profession != "" && Validation.isNotAllowSpecialCharacters(profession) )
		{
			this.setState({ validate_of_profession: false, edit_of_profession: true, is_saved: true });
			result = false;
		}
		else
		{
			this.setState({ validate_of_profession: true });
		}

		// Organization
		if( organization != "" && Validation.isNotAllowSpecialCharacters(organization) )
		{
			this.setState({ validate_of_organization: false, edit_of_organization: true, is_saved: true });
			result = false;
		}
		else
		{
			this.setState({ validate_of_organization: true });
		}
		
		// Designation
		if( designation != "" && Validation.isNotAllowSpecialCharacters(designation) )
		{
			this.setState({ validate_of_designation: false, edit_of_designation: true, is_saved: true });
			result = false;
		}
		else
		{
			this.setState({ validate_of_designation: true });
		}

		// Department
		if( department != "" && Validation.isNotAllowSpecialCharacters(department) )
		{
			this.setState({ validate_of_department: false, edit_of_department: true, is_saved: true });
			result = false;
		}
		else
		{
			this.setState({ validate_of_department: true });
		}

		_scrollView.scrollTo({x: 0, y: 0, animated: true})

		return result;
	}

	handleSaveProfile = () => {

		let is_validate = this._form_validation();

		if( is_validate )
		{
			this._handleProfileUpdateAjax({
				
				user_id: this.state.user_id,
				first_name: this.state.first_name,
				last_name: this.state.last_name,
				// username: this.state.username,
				// email: this.state.email,
				mobile: this.state.mobile,
				address: this.state.address,
				date_of_birth: this.state.date_of_birth,
				// gender: this.state.gender,
				avatar: this.state.avatar,
				
				// city: this.state.city,
				qualification: this.state.qualification,
				profession: this.state.profession,
				organization: this.state.organization,
				designation: this.state.designation,
				department: this.state.department
			});
		}
	}

	_handleProfileUpdateAjax = (postdata) => {

		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/profile", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{
				this._handleOfResetForm();
				this.getUserProfile( this.state.user_id );
			}

			this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
			});
		})
		.catch(function(error){
			console.warn(error);
		})
		.done();
	}

	_handleOfResetForm()  
	{
		this.setState({
			
			edit_of_fullname: false,
			edit_of_mobile: false,
			edit_of_date_of_birth: false,
			edit_of_address: false,
			edit_of_qualification: false,
			edit_of_profession: false,
			edit_of_organization: false,
			edit_of_designation: false,
			edit_of_department: false,

			is_saved: false
		});
	}

	getUserProfile = ( user_id ) => {
		
		this.setState({ isLoading: true });
		
		let api_url_by_profile = (Config.base_url() + "/api/data/profile?user_id=" + user_id );
		
		fetch( api_url_by_profile, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });
			
			if( res.status )
			{
				let profile = res.data.user;
				let avatar 	= profile.avatar_url;

				AsyncStorage.setItem("name", profile.first_name +' '+ profile.last_name );
				AsyncStorage.setItem("email", profile.email );
				AsyncStorage.setItem("avatar_url", avatar );

				this.setState({
					first_name: profile.first_name,
			        last_name: profile.last_name,
			        username: profile.username,
			        email: profile.email,
			        mobile: profile.mobile,
			        date_of_birth: profile.date_of_birth,
			        address: profile.address,
			        gender: profile.gender,

			        city: profile.region,
                    qualification: profile.qualification,
                    profession: profile.profession,
                    organization: profile.organization,
                    designation: profile.designation,
                    department: profile.department,
					avatar: profile.avatar,
					avatar_url: profile.avatar ? Config.base_url() + profile.avatar_url : '',
				});
			}
			else
			{
				this.setState({
					isMessageModal: true,
					isMessageModalText: res.message
				})
			}

		})
		.catch(error => console.log(error))
        .done();
   }

    render()
    {
    	const {navigate} = this.props.navigation;
    	
        return(

			<KeyboardAvoidingView  behavior="padding" enabled style={{
				flex: 1
			  }}>
			<ImageBackground source={require('./../img/common-background.png')} style={{flex:1}}>
				
				<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

				<Container style={{ backgroundColor:'transparent'}}>
					<Header style={{ backgroundColor:'#ed3134' }} >
						<Left>
							<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
								<Image  source={ require('./../img/icon/menu.png') } style={{ height:20, width:20 }} />
							</TouchableOpacity>
						</Left>
						<Body>
							<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Profile</Title>
						</Body>
						<Right>
		                <TouchableOpacity>
		                  <Button transparent onPress={() => navigate('Home') }>
		                      <Image 
		                      source={ require('./../img/icon/arrow.png') }
		                      style={{ height:12, width:22 }}
		                    />
		                  </Button>
		                </TouchableOpacity>
		              </Right>
					</Header>
					<Content contentContainerStyle={ styles.bodyContainer }>

						<View style={styles.topHeader}>
							
							<Image
								style={{
									width: 100,
									height: 100,
									borderRadius: 50,
									marginTop: 15
								}}
								resizeMode='cover'
								source={{
									uri: (this.state.avatar_url ? this.state.avatar_url : 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg')
								}}
							/>
							<Text style={styles.mainText}>Personal Detail</Text>
							<View style={styles.iconPencil}>
							    <TouchableOpacity onPress = {  this.hanldeOfUploadYourQuery.bind(this) } >
								    <Image source={require('./../img/icon/id-pen-icon.png')} style={{ width:13, height:15, margin:9 }} />
								</TouchableOpacity>
							</View>
						</View>

						<ScrollView
							ref={scrollView => this.scrollView = scrollView}
							keyboardShouldPersistTaps="always"
						>
							<View style={{}}>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Full Name</Text>
									<View style={styles.header3}>
										{ this.state.edit_of_fullname ? 
											(<View>
												<TextInput 
													style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_first_name ? styles.err:null] }
													underlineColorAndroid='transparent'
													placeholder= 'First Name'
													placeholderTextColor= '#000'
													// value={ this.state.first_name }
													onChangeText={(text) => this.setState({'first_name': text})}
													minLength={5}
													maxLength={20}
												/>
												<TextInput 
													style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_last_name ? styles.err:null] }
													underlineColorAndroid='transparent'
													placeholder= 'Last Name'
													placeholderTextColor= '#000'
													// value={ this.state.last_name }
													onChangeText={(text) => this.setState({'last_name': text})}
													minLength={5}
													maxLength={20}
												/>
											</View>)
										:
										(<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { this.state.first_name + ' ' + this.state.last_name }
										</Text>)
										}

										{ !this.state.edit_of_fullname  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_fullname: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Username</Text>
									<View style={styles.header3}>
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { this.state.username }
										</Text>
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Email</Text>
									<View style={styles.header3}>
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { this.state.email }
										</Text>
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Mobile</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_mobile ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_mobile ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Mobile'
								            keyboardType = 'numeric'
								            placeholderTextColor= '#000'
								            // value={ this.state.mobile }
								            onChangeText={(text) => this.setState({'mobile': text})}
								            maxLength={15}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { this.state.mobile }
										</Text> 
										}

										{ !this.state.edit_of_mobile  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_mobile: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Date of Birth</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_date_of_birth ? 
										<DatePicker 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_date_of_birth ? styles.err : null ] }
											mode="date"
								            showIcon={false}
								            date={this.state.date_of_birth}
								            value={ this.state.date_of_birth }
								            placeholder= 'Date of Birth'
								            format="DD-MM-YYYY"
											confirmBtnText="Confirm"
											cancelBtnText="Cancel"
								            customStyles={{
								            	dateIcon: {
								            		width: 0, 
								            		height: 0
								            	},
								            	dateInput: {
								            		borderWidth: 0,
								            		alignItems: 'flex-start'
								            	},
								            	placeholderText: {
								            		fontFamily: 'Roboto-Bold',
								            		fontWeight: '500',
								            		fontSize: 13,
								            		color: '#000'
								            	},
								            	dateText: {
								            		fontFamily: 'Roboto-Bold',
								            		fontWeight: '500',
								            		fontSize: 13,
								            		color: '#000'
								            	}
								            }}
								            onDateChange={(date) => {this.setState({date_of_birth: date})}}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { this.state.date_of_birth }
										</Text> 
										}

										{ !this.state.edit_of_date_of_birth  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_date_of_birth: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Address</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_address ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_address ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Address'
								            placeholderTextColor= '#000'
								            // value={ this.state.address }
								            onChangeText={(text) => this.setState({'address': text})}
								            minLength={3}
								            maxLength={100}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { (this.state.address || '-') }
										</Text> 
										}
										
										{ !this.state.edit_of_address  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_address: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>City</Text>
									<View style={styles.header3}>
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { this.state.city }
										</Text>
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Qualification</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_qualification ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_qualification ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Qualification'
								            placeholderTextColor= '#000'
								            // value={ this.state.qualification }
								            onChangeText={(text) => this.setState({'qualification': text})}
								            minLength={3}
								            maxLength={50}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { (this.state.qualification || '-') }
										</Text> 
										}

										{ !this.state.edit_of_qualification  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_qualification: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Profession</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_profession ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_profession ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Profession'
								            placeholderTextColor= '#000'
								            // value={ this.state.profession }
								            onChangeText={(text) => this.setState({'profession': text})}
								            minLength={3}
								            maxLength={50}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { (this.state.profession || '-') }
										</Text> 
										}

										{ !this.state.edit_of_profession  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_profession: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Organization</Text>
									<View style={styles.header3}>
									   { this.state.edit_of_organization ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_organization ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Organization'
								            placeholderTextColor= '#000'
								            // value={ this.state.organization }
								            onChangeText={(text) => this.setState({'organization': text})}
								            minLength={3}
								            maxLength={50}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { (this.state.organization || '-') }
										</Text> 
										}
										
										{ !this.state.edit_of_organization  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_organization: true, is_saved: true})}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Designation</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_designation ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_designation ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Designation'
								            placeholderTextColor= '#000'
								            // value={ this.state.designation }
								            onChangeText={(text) => this.setState({'designation': text})}
								            minLength={3}
								            maxLength={50}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { (this.state.designation || -'') }
										</Text> 
										}

										{ !this.state.edit_of_designation  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_designation: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Department</Text>
									<View style={styles.header3}>
										
										{ this.state.edit_of_department ? 
										<TextInput 
										    style={ [{width:286, backgroundColor: 'rgba(152, 152, 148, 0.7)', paddingVertical:10, paddingHorizontal:12, marginTop:5, color:'black', borderRadius:10}, !this.state.validate_of_department ? styles.err : null ] }
											underlineColorAndroid='transparent'
								            placeholder= 'Department'
								            placeholderTextColor= '#000'
								            // value={ this.state.department }
								            onChangeText={(text) => this.setState({'department': text})}
								            minLength={3}
								            maxLength={50}
									    />
										: 
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    { (this.state.department || '-') }
										</Text> 
										}

										{ !this.state.edit_of_department  ?
										<View>
											<TouchableOpacity onPress={ () => this.setState({ edit_of_department: true, is_saved: true })}>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
										: null }
									</View>
								</View>

								<View style={styles.contentContainer }>
									<Text style={{fontSize:16, textAlign:'left', width:300, marginTop:15, fontFamily:'Roboto-Regular', fontWeight:'bold' }}>Password</Text>
									<View style={styles.header3}>
										<Text style={{width:286, color:'#ababab', fontSize:14}}>
										    *****
										</Text>
										<View>
											<TouchableOpacity onPress={() => navigate('ChangePassword') }>
												<Image source={require('./../img/icon/edit-icon.png')} style={{ width:16, height:16}} />
											</TouchableOpacity>
										</View>
									</View>
								</View>
								
							</View>

							{ this.state.is_saved ? 
							<View style={{  marginVertical: 30 }}>
								   <TouchableOpacity style={{ width: 310, backgroundColor: '#ee3135', borderRadius: 25, paddingVertical: 12 }} >
						            	<Text 
						            		style={{ fontSize: 16, fontWeight: '500', color:'#ffffff', textAlign: 'center' }}
						            		onPress = {() => this.handleSaveProfile() } 
						            	>Save</Text>
						            </TouchableOpacity>
					            </View>
							: null }

						</ScrollView>
					</Content>

					<Footer style={{ backgroundColor:'transparent' }}>
						<FooterCopyRight />
					</Footer>

					<Modal
						transparent={ true }
						visible={this.state.isMessageModal}
						onRequestClose={ () => console.warn('this is') }
						>
							<View style={ customStyles.modalBody }>
								<View style={ customStyles.modalContainer }>
									<View style={ customStyles.modalBox }>
										<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
												this.setState({
													isMessageModal: false
												})
											}}>
												<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										<Text style={ customStyles.modalTextMessage }>
											{ this.state.isMessageModalText }
										</Text>
									</View>
								</View>
							</View>
						</Modal>

				</Container>
			</ImageBackground>
			</KeyboardAvoidingView>
		);
    };
}