import React from 'react';
import { Text, View,  Image, ImageBackground,  StyleSheet, TextInput, TouchableOpacity,  AsyncStorage, Modal } from 'react-native';
import { Header, Content, Footer, Container, Left, Body, Right, Title, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker';
import {Select, Option} from "react-native-chooser";

import * as Animatable from 'react-native-animatable';
import FCM, {FCMEvent} from "react-native-fcm";

import Config from './../Services/Config';
import Validation from './../Services/Validation';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

import customStyles from './../styles/Custom';

export default class GetAnAppoinment extends React.Component {

  	constructor(props)
	{
		super(props);

		this.btnFormSubmit = this.btnFormSubmit.bind(this);

		this.state = {

			user_id: 0,
			
			validate_of_booking_id: true,
			validate_of_booking_id_msg: '',
			booking_id: '',
			booking_title: 'I Want To Discuss About',

			validate_of_booking_date: true,
			validate_of_booking_date_msg: '',
			booking_date: '',

			validate_of_booking_time: true,
			validate_of_booking_time_msg: '',
			booking_time: '',

			validate_of_details: true,
			validate_of_details_msg: '',
			details: '',

			isLoading: false,

			isMessageModal: false,
			isMessageModalText: '',

			getFilters: []
		};

		AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));

		FCM.on(FCMEvent.Notification, (notif) => {
           this.setState({ isMessageModal: true, isMessageModalText: notif.body })
        });
	};

	_form_validation = () => {

		let booking_id 			= this.state.booking_id;
		let booking_date 		= this.state.booking_date;
		let booking_time 		= this.state.booking_time;
		let details 			= this.state.details;

		this.setState({ validate_of_booking_id: ((booking_id != "") ? true : false) });
		this.setState({ 
			validate_of_booking_id_msg: ((booking_id == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_booking_date: ((booking_date != "" ) ? true : false) });
		this.setState({ 
			validate_of_booking_date_msg: ((booking_date == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_booking_time: ((booking_time != "" ) ? true : false) });
		this.setState({ 
			validate_of_booking_time_msg: ((booking_time == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_details: ((details != "" && !Validation.isNotAllowSpecialCharacters(details) ) ? true : false) });
		this.setState({ validate_of_details_msg: (
			(details == "" ) ? 
			'This field is required' : 
			( Validation.isNotAllowSpecialCharacters(details) ? 'This format is invalid' : '')
		) });
	}

	btnFormSubmit = () => {
		
		this._form_validation();

		let is_validate = ( 
			(this.state.validate_of_booking_id && this.state.booking_id ) &&
			( this.state.validate_of_booking_date && this.state.booking_date) &&
			(this.state.validate_of_booking_time && this.state.booking_time) &&
			(this.state.validate_of_details && this.state.details)
		);
		
		if( is_validate )
		{
			let postdata = {
				user_id: this.state.user_id,
				booking_id: this.state.booking_id,
				booking_date: this.state.booking_date,
				booking_time: this.state.booking_time,
				details: this.state.details
            }

			this.ajaxPostFormSubmit( postdata );
		}
	};

	ajaxPostFormSubmit( postdata )
	{
		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/book-appoinment", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{
				this.setState({
					booking_id: '',
					booking_title: 'I Want To Discuss About',
					booking_date: '',
					booking_time: '',
					details: ''
				});
			}

			this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
			});
		})
		.catch( (error) => console.warn(error) )
		.done();
	}

	componentDidMount()
	{
		this._getFilters();
	};
	
	_getFilters = () => {

		fetch( Config.base_url() + "/api/data/filters?type=appointment", {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then((response) => response.json() )
		.then((res) => {

			if( res.status )
			{
				let filters 		= res.data;
				let first_element  	= ( filters.length ? filters[0] : '' );

				this.setState({ 
					// booking_title: ( first_element.title || '' ),
					// booking_id: ( first_element.id || '' ),
					getFilters: filters
				});
			}
		})
		.catch( (error) => console.warn(error) )
		.done();
	}
	
	render()
	{
		return (
			<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

				<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

				<Container style={{ backgroundColor:'transparent'}}>
					<Header style={{ backgroundColor:'#ed3134' }} >
						<Left>
							<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
								<Image 
									source={ require('./../img/icon/menu.png') }
									style={{ height:20, width:20 }}
								/>
							</TouchableOpacity>
						</Left>
						<Body>
							<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Schedule a Meeting</Title>
						</Body>
						<Right>
			                <TouchableOpacity>
			                  <Button transparent onPress={() => this.props.navigation.navigate('Support Hub')}>
			                      <Image 
			                      source={ require('./../img/icon/arrow.png') }
			                      style={{ height:12, width:22 }}
			                    />
			                  </Button>
			                </TouchableOpacity>
			            </Right>
					</Header>
					<Content contentContainerStyle={ styles.bodyContainer }>

						<View style={styles.bodySubContainer}>
								
								<Animatable.View animation="flipInX">
									<View 
										style={ [styles.inputDateField, { flexDirection: 'row'}, !this.state.validate_of_booking_id ? styles.err:null ] } 
									>
										<Select
											onSelect = { (value, text) => this.setState({ booking_id: value, booking_title: text })}
											defaultText  = { this.state.booking_title }
											style = {{ borderWidth : 1, borderColor : "#5a5a58", width: 260 }} 
											textStyle = {{ fontFamily: 'Roboto-Regular', fontSize: 13, marginLeft:-10, color: '#fff', }}
											optionListStyle = {{ borderColor: 'transparent', backgroundColor: '#fff', height: 250 }}
											transparent={true}
										>
											<Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} value= "">I Want To Discuss About</Option>
											{ this.state.getFilters.map( (value, index) => <Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} key={index} value={value.id}>{value.title}</Option> ) }
										</Select>
										<Image 
												source={ require('./../img/icon/dropdown.png') }
												style={{ height:10, width:15, top: 15 , left:-20}}
											/>
									</View>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_booking_id ? customStyles.usama:null }>{ this.state.validate_of_booking_id_msg }</Text>
								</View>

								<Animatable.View animation="flipInX">
									<DatePicker
										style={ [styles.inputDateField, !this.state.validate_of_booking_date ? styles.err:null] } 
										key={1}
										date={this.state.booking_date}
										mode="date"
										value={ this.state.booking_date}
										placeholder="Select Date"
										format="YYYY-MM-DD"
										confirmBtnText="Confirm"
										cancelBtnText="Cancel"
										iconSource={ require('./../img/icon/calendar.png') }
										customStyles={{
											dateIcon: {
												position: 'absolute',
												right: 0,
												top: 11,
												height:18,
												width:16
											},
											dateInput: {
												borderWidth: 0,
												alignItems: 'flex-start',
											},
											placeholderText: {
												fontSize: 13,
												color: '#fff',
											},
											dateText: {
												fontSize: 13,
												color: '#fff'
											}
										}}
										onDateChange={(date) => {this.setState({booking_date: date})}}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_booking_date ? customStyles.usama:null }>{ this.state.validate_of_booking_date_msg }</Text>
								</View>
								
								<Animatable.View animation="flipInX">
									<DatePicker
										style={ [styles.inputDateField, !this.state.validate_of_booking_time ? styles.err:null] } 
										date={this.state.booking_time}
										key={2}
										mode="time"
										value={ this.state.booking_time}
										placeholder="Select Time"
										format="h:mm:ss a"
										confirmBtnText="Confirm"
										cancelBtnText="Cancel"
										iconSource={ require('./../img/icon/time-icon.png') }
										customStyles={
											{
												dateIcon: {
													position: 'absolute',
													right: 0,
													top: 12,
													height:15,
													width:15
												},
												dateInput: {
													alignItems: 'flex-start',
													borderWidth: 0

												},
												placeholderText: {
													color: '#ffffff'
												},
												placeholderText: {
												fontSize: 13,
												color: '#fff',
												},
												dateText: {
												fontSize: 13,
												color: '#fff'
												}
											}
										}
										onDateChange={(time) => {this.setState({booking_time: time})}}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_booking_time ? customStyles.usama:null }>{ this.state.validate_of_booking_time_msg }</Text>
								</View>

								<Animatable.View animation="flipInX">
									<TextInput
										style={ [styles.fieldTextarea, !this.state.validate_of_details ? styles.err:null] } 
										multiline={true}
										numberOfLines={6}
										minHeight={150}
										maxHeight={150}
										underlineColorAndroid='transparent'
										placeholder= 'Enter Details'
										placeholderTextColor= '#ffffff'
										// value={ this.state.details}
										onChangeText = { (text) => this.setState({ details: text }) }
										minLength={10}
										maxLength={250}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_details ? customStyles.usama:null }>{ this.state.validate_of_details_msg }</Text>
								</View>

						</View>

						<Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 30 }}>
							<Button 
								style={styles.touchBtnSubmit}
								onPress = {() => this.btnFormSubmit()}
							>
								<Text style={styles.btnButtonSubmit}>Submit</Text>
							</Button>
						</Animatable.View>
					</Content>

					<Footer style={{ backgroundColor:'transparent' }}>
						<FooterCopyRight />
					</Footer>


					<Modal
						transparent={ true }
						visible={this.state.isMessageModal}
						onRequestClose={ () => console.warn('this is') }
					>
						<View style={ customStyles.modalBody }>
							<View style={ customStyles.modalContainer }>
								<View style={ customStyles.modalBox }>
									<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
											this.setState({
												isMessageModal: false
											})
										}}>
											<Icon name="times-circle" size={18} color="white" />
									</TouchableOpacity>
									<Text style={ customStyles.modalTextMessage }>
										{ this.state.isMessageModalText }
									</Text>
								</View>
							</View>
						</View>
					</Modal>

				</Container>
		</ImageBackground>
		);
	} 
}

const styles = StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},

	inputField: {
		width: 300,
		backgroundColor: '#5a5a58',
		borderRadius: 25,
		paddingHorizontal: 16,
		fontSize: 16,
		color: '#ffffff',
		marginVertical: 5
	},

    inputDateField: {
        width: 300,
		backgroundColor: '#5a5a58',
		borderRadius: 25,
		paddingHorizontal: 20,
		paddingVertical:4,
		marginVertical: 5
    },

	fieldTextarea: {
		width: 300,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		fontSize: 16,
		color: '#ffffff',
		paddingHorizontal: 20,
		paddingVertical:12,
		marginVertical: 5
    },
    
    dateDropDownPicker: {
        color: '#ffffff',
        // borderRadius: 14,
        borderWidth: 1,
        backgroundColor: '#5a5a58',
        width: 320,
        borderRadius: 25,
        marginVertical: 5
    },

	touchBtnSubmit: {
		width: 300,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 5,
		paddingVertical: 12
	},

	btnButtonSubmit: {
		width: 300,
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},
    
    err: {
		borderWidth:1,
		borderColor: 'red'
	}
});