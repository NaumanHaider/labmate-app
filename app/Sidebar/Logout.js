import React from 'react';
import {Text, View, Image, ImageBackground, StyleSheet, AsyncStorage} from 'react-native';
import {
  
  Header, 
  Content, 
  Footer,

  Container, 
  Left, 
  Body, 
  Right, 

  Icon, 
  Button, 
  Title,

  Card,
  CardItem
  
} from 'native-base';

export default class Aboutus extends React.Component {
  
  static navigationOptions = {
    drawerIcon: (
      <Image 
        source={ require('./../img/icon/log-out-icon.png') }
        style={{ height:20, width:20 }}
      />
    )
  }

  constructor()
  {
      super();
  };

  render(){

    const { navigate } = this.props.navigation;

    AsyncStorage.removeItem("is_login");
    AsyncStorage.setItem("user_id", "");
    AsyncStorage.removeItem("name");
    AsyncStorage.removeItem("email" );
    AsyncStorage.removeItem("avatar_url");
          
    navigate('Login');

    return true;
  };
  
}
