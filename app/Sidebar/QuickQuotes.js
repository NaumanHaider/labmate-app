import React from 'react';
import { Text, View, Image, ImageBackground, TextInput, TouchableOpacity, AsyncStorage, FlatList, Modal} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right, Title, Button} from 'native-base';


import Icon from 'react-native-vector-icons/FontAwesome';
import {Select, Option} from "react-native-chooser";
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-picker';

import * as Animatable from 'react-native-animatable';

import Config from './../Services/Config';
import Validation from './../Services/Validation';

// Styles & Formatting
import styles from './../styles/QuickQuotos';
import customStyles from './../styles/Custom';

// Components
import ProductItem from './../components/QuickQuote/ProductItem';
import ProductFileItem from './../components/QuickQuote/ProductFileItem';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

const options = {
  title: 'Select File',
  storageOptions: {
    skipBackup: true,
    mediaType: 'photo',
    path: 'images'
  }
};

export default class QuickQuotes extends React.Component
{
  constructor()
  {
    super();

    this.state = {

      user_id: 0,

      category_title: '',
      category_id: 0,

      search_product: '',
      validate_of_product_search: true,
      validate_of_product_search_msg: '',
      
      details: '',
      validate_of_details: true,
      validate_of_details_msg: '',

      //Loading
      isLoading: false,
      refreshing: false,
      btnAddToCartDisable: true,

      isImageModal: false,
      isImageSourceURL: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg',

      //Modals
      isMessageModal: false,
      isMessageModalText: '',

      isMessageModal1: false,
      isMessageModalText1: '',

      // Only Fetch Records
      get_all_categories: [],
      get_products: [],
      cart_products: [],
      selection_products: [],
      selection_files: [],
      getUserSelections: [],

      getFilters: []
    }

    AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));
    AsyncStorage.getItem('user_id').then((value) => this.getUserSelections( value ) );
  };

  componentDidMount()
	{
    this.getAllCategories();
  };
  
  getAllCategories = () => {
    
    let api_url_by_categories = (Config.base_url() + "/api/data/search-categories");

		fetch( api_url_by_categories, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

        this.setState({ 
          get_all_categories: res.data
        });
		})
		.catch(error => console.log(error))
    .done();
  }

  getUserSelections = ( user_id ) => {
    
    this.setState({ isLoading: true });
    
    let api_url_by_select_products = (Config.base_url() + "/api/data/selection-products?user_id=" + user_id );
    
    fetch( api_url_by_select_products, {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then( (response) => response.json() )
    .then( (res) => {

      this.setState({ isLoading: false });

      let fetch_length   = Object.keys(res.data).length;

      if( res.status && fetch_length)
      {
        let fetch = res.data.quick_quotes;

        this.setState({ 
          details: fetch.description
        });

        // Exists of Products
        if( fetch.products && Object.keys(fetch.products).length )
        {
          let getCarts = fetch.products.map( (value) => value.id );
          this.setState({ cart_products: getCarts });
        }

        // Exists of Files
        if( fetch.files && Object.keys(fetch.files).length )
        {
          userSelectionFiles = [];

          for (let file of fetch.files)
          {
            // console.warn( file );
            let _userSelectionFile = {
              id: file.id,
              file_name: file.source_name,
              file_url: Config.base_url() + file.file_url,
              source: file.source
            }

            userSelectionFiles.push( _userSelectionFile );
          };

          this.setState({ selection_files: userSelectionFiles });
        }
        
        this.setState({ getUserSelections: fetch.quick_quotes });
      }
    })
    .catch(error => console.log(error))
    .done();
  }

  _handleSelectProductByChecked = ( id, index  ) => {
    
    let get_products        = this.state.get_products;
    let cart_products       = this.state.cart_products;
    
    if( get_products[index] )
    {
      let is_checked    = get_products[index].is_checked;
      let check_status  = is_checked ? false : true;

      get_products[index].is_checked = check_status; 

      if( check_status ) {
        cart_products.push(id);
      } else {
        cart_products.splice(cart_products.map( (x,y) => ( id==x ? y : 0)).filter(x=>x)[0], 1);
      }

      this.setState({
        get_products: get_products,
        cart_products: cart_products
      });
    }

    this.setState({ btnAddToCartDisable: true });

    // Button :: Enable Add to Cart
    if( this.state.cart_products.length ) {
      this.setState({ btnAddToCartDisable: false });
    }
  }
  
  /**
   * Search Production By Categories
   * - - - - - - - - - - - - - - - - 
   * 
   */
  onHandleSearchProduct = () =>  {
    
    let category_id = this.state.category_id;
    let keyword     = this.state.search_product;
    
    this.setState({ validate_of_product_search: true, validate_of_product_search_msg: '' });
    
    if( keyword.length < 3 ) 
    {
      this.setState({
        validate_of_product_search: false,
        validate_of_product_search_msg: 'Type atleast 3 characters to proceed.'
      });
    }
    else
    {
      let api_url_by_product = (Config.base_url() + "/api/data/search-products?category_id=" + category_id + '&keyword=' + keyword );
    
      this.setState({ isLoading: true });

      fetch( api_url_by_product, {
        method: "GET",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      .then( (response) => response.json() )
      .then( (res) => {
        
        this.setState({ isLoading: false });

        let data = res.data;

        if( data.length < 1 )
        {
          this.setState({
            category_id: 0,
            category_title: '',
            
            search_product: '',
            get_products: [],

            isMessageModal: true,
            isMessageModalText: 'We cannot find your searched item, Please write in Requirement box'
          });
        }

        // let get_user_selection = {"18":{"checked":false},"19":{"checked":true},"20":{"checked":true},"21":{"checked":false}};
        
        if( data.length )
        {
          
          let carts       = this.state.cart_products;
          let _products   = [];

          // console.warn( `Carts: ${carts}` );
          
          for(let i = 0; i < data.length ; i++)
          {
            let is_checked  = false;
            let getSingleProduct = data[i];

            // Exists of Cart IDS (Checked OR UnChecked)
            if( carts.length )
            {
              for (let cart_id of carts) {
                // console.warn(`Cart ID: ${cart_id}, Product ID: ${getSingleProduct.id}`)
                if( cart_id == getSingleProduct.id){

                  this.setState({ btnAddToCartDisable: false });
                  is_checked = true;
                }
              }

              // console.warn(`Product ID: ${getSingleProduct.id}, is_checked: ${is_checked}`)
            }

            getSingleProduct.is_checked = is_checked;

            _products.push( getSingleProduct );

            // is_checked = (this.onHandleCartProductInArray(set_product_id, carts) ? true : false);
            // console.warn( `is_checked: ${is_checked}` );

          }
          
          // this.setState({ selection_products: set_selection_products });
          this.setState({ get_products: _products });
        }
      })
      .catch((error) => console.warn(error))
      .done();
    }
  }

  /**
   *
   * Referesh Products
   * - - - - - - - - - - - - - - -
   * 
   */
  onHandleRefreshProducts = () => {

    this.setState({ 
      category_title: 'Choose Product Line',
      category_id: 0,

      search_product: '',
      get_products: [], 
      cart_products: [],
      selection_products: [] 
    });
  }

  /**
   *
   * Action for Add to Cart
   * - - - - - - - - - - - - - - -
   * 
   */
  onHandleAddToCart = () => {

    let products            = this.state.cart_products || 0;
    let is_checked_products = this.state.get_products.map( (value) => {
      return (value.is_checked ? 1 : 0);
    }).filter( (x) => x );

    //Submit of Cart
    if( is_checked_products.length > 0 )
    {
      let postdata = {
        user_id: this.state.user_id,
        action: 'addtocart',
        product_id: products
      }

      this.setState({ isLoading: true });

      fetch( Config.base_url() + "/api/user/quick-quote-enquiry", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(postdata)
      })
      .then((response) => response.json() )
      .then((res) => {

        this.setState({ 
          isLoading: false,
        });

        if( res.status )
        {
          this.setState({
            category_title: '',
            category_id: 0,

            search_product: '',

            get_products: []
          });
          
          this.setState({
            isMessageModal1: true,
            isMessageModalText1: 'Items added to RFQ, To view the items go to Preview'
          });

          setTimeout( () => {
            
            this.setState({
              isMessageModal1: false, isMessageModalText1: ''
            });
            
          }, 3000);
          
        }
        else
        {
          this.setState({
            isMessageModal: true,
            isMessageModalText: res.message
          });
        }
      })
      .catch( (error) => console.warn(error) )
      .done();
    }
  }

  /**
   * Form Validation
   * - - - - - - - - -
   * 
   */
  handleOfFormValidate = () => {

    let details               = this.state.details || '';
    let products              = this.state.cart_products || 0;
    let files                 = this.state.selection_files || 0;
    let is_products           = true;
    let is_files              = true;
    let result                = true;
    
    // Validate of Products
    if( products.length < 1 )
    {
      is_products = false;
    }

    // Validate of Files
    if( files.length < 1 && details == "" )
    {
      is_files  = false;
    }

    if( is_files == false && is_products == false )
    {
      this.setState({
        isMessageModal: true,
        isMessageModalText: `Please fill any one: 1=Product, 2=Requirement`
      });

      result = false;
    }
    else
    {

      if( result && is_products == false && is_files )
      {
        this.setState({ validate_of_details_msg: (
          (details == "" ) ? 
          'This field is required' : 
          ( Validation.isNotAllowSpecialCharacters(details) ? 'This format is invalid' : '')
        )});

        this.setState({ validate_of_details: ((details != "" && !Validation.isNotAllowSpecialCharacters(details) ) ? true : false) });

        if( this.state.validate_of_details == false && files.length < 1 )
          result = false;
      }
    }

    return result;
  }

  /**
   *
   * Preview && Save (Requirements OR Attachment Files)
   * - - - - - - - - - - - - - - - - - - - - - - - - - 
   * 
   */
  onHandleReviewSelect = (navigate) => {

    /*let is_validate = this.handleOfFormValidate();
    
    //Submit of Cart
    if( is_validate )
    {
      
    }*/

    let postdata = {
      user_id: this.state.user_id,
      description: this.state.details,
      sources: this.state.selection_files
    }
    // console.warn( postdata );
    
    this.ajaxFormUserProductSaved(postdata, navigate);
  }

  ajaxFormUserProductSaved = (postdata, navigate) => {
    
    this.setState({ isLoading: true });

    fetch( Config.base_url() + "/api/user/quick-quote-enquiry", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

      this.setState({ 
        isLoading: false,
      });
      
			if( res.status )
			{
				this.setState({
          details: '',
					get_products: []
        });
        
        navigate('YourSelection');
      }
      else
      {
        this.setState({
          isMessageModal: true,
          isMessageModalText: res.message
        });
      }

		})
		.catch( (error) => console.warn(error) )
		.done();
  }
  
  hanldeOfUploadYourQuery = () => {

    let _getTempUploadQuery = this.state.selection_files;

    ImagePicker.showImagePicker(options, (response) => {
    // console.warn('Response = ', response);

      if (response.didCancel) {
        // console.warn('User cancelled image picker');
      }
      else if (response.error) {

        this.setState({
          isMessageModal: true,
          isMessageModalText: response.error
        });
        // console.warn('ImagePicker Error: ', response.error);
      }
      else 
      {
        let setTempQuery = {
          file_name: response.fileName,
          file_url: response.uri,
          source: response.data
        }

        _getTempUploadQuery.push( setTempQuery );
        this.setState({ selection_files: _getTempUploadQuery });

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
      }
    });
  }

  hanldeOfTempDelFiles = ( index ) => {
    
    let getFiles = this.state.selection_files;
    
    // delete from remote server
    if( getFiles[index] && getFiles[index].id ){
      this._deleteFileOnRemote( getFiles[index].file_name );
    }

    getFiles.splice(index, 1);

    this.setState({
      selection_files: getFiles,
      isMessageModal: true,
      isMessageModalText: 'Delete File Successfully'
    });
  }

  /**
   * Remote :: File Deleted
   * - - - - - - - - - - -
   */
  _deleteFileOnRemote = ( id ) => {

    this.setState({ isLoading: true });
    
    let postdata = {
      user_id: this.state.user_id,
      source: id
    };
    
    fetch( Config.base_url() + "/api/data/delete-selection-file", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(postdata)
    })
    .then((response) => response.json() )
    .then((res) => {
      
      this.setState({ isLoading: false });
        
      if( res.status != true )
      {
        this.setState({ 
          isMessageModal: true,
          isMessageModalText: res.message
        });
      }
    })
    .done();
  }

  _display_of_modal_image = ( source ) => {
    
    let image_source = source;

    this.setState({
      isImageModal: true,
      isImageSourceURL: image_source
    });
  }

  render()
  {
    const {navigate} = this.props.navigation;
    
      return (
        <ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

        <Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

          <Container style={{ backgroundColor:'transparent'}}>
            <Header style={{ backgroundColor:'#ed3134' }} >
              <Left>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
                  <Image 
                    source={ require('./../img/icon/menu.png') }
                    style={{ height:20, width:20 }}
                  />
                </TouchableOpacity>
              </Left>
              <Body>
                <Title style={{color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Request For Quotation</Title>
              </Body>
              <Right>
                <TouchableOpacity>
                  <Button transparent onPress={() => navigate('Home') }>
                      <Image 
                      source={ require('./../img/icon/arrow.png') }
                      style={{ height:12, width:22 }}
                    />
                  </Button>
                </TouchableOpacity>
              </Right>
            </Header>
            <Content contentContainerStyle={ styles.bodyContainer }>
            <Animatable.Text animation="flipInX" style={styles.mainText}>Search Product</Animatable.Text>
              <View style={styles.bodySubContainer}>
                  
                  <Animatable.View animation="flipInX">
                    <Select
                      onSelect = { (value, text) => this.setState({ category_title: text, category_id: value })}
                      defaultText  = { this.state.category_title || 'Choose Product Line' }
                      style = {[
                        {width: 270, borderRadius:25, backgroundColor: '#5a5b58', paddingVertical:12, paddingHorizontal:20}
                      ]} 
                      textStyle = {{ fontFamily: 'Roboto-Bold', fontWeight: '500', color: '#fff', fontSize: 16 }}
                      optionListStyle = {{ borderColor: 'transparent', backgroundColor: '#fff', height: 250 }}
                      transparent={true}
                    >
                      <Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} value= "0">Choose Product Line</Option>
                      {
                        this.state.get_all_categories.map((x, index) => {
                          return <Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} key={index} value= { x.id }>{ x.title }</Option>;
                        })
                      }
                    </Select>
                    <Image 
                      source={ require('./../img/icon/dropdown.png') }
                      style={{ height:10, width:16, position:'absolute', top:18, right:20 }}
                    />
                  </Animatable.View>

                  <Animatable.View animation="flipInX">
                    <TextInput 
                      style={ [styles.inputField, { width: 265}] } 
                      underlineColorAndroid='transparent'
                      placeholder= 'Product Name'
                      /*value={ this.state.search_product }*/
                      onChangeText = { (text) => this.setState({ search_product: text }) }
                      placeholderTextColor= '#ffffff'
                    />
                  </Animatable.View>
                  <View>
                  <Text></Text>
                    <Text style={ !this.state.validate_of_product_search ? customStyles.usama:null }>{ this.state.validate_of_product_search_msg }</Text>
                  </View>

                  <Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 10 }}>
                    <Button 
                      style={[styles.touchBtnSubmit, {width: 150}]}
                      onPress = {() => this.onHandleSearchProduct()}
                    >
                      <Text style={[styles.btnButtonSubmit, {width: 150, textAlign: 'center' }]}>Search</Text>
                    </Button>
                  </Animatable.View>

                  {/* Search Products */}
                  <View style={{ paddingVertical: 20, flex:1 }}>
                    <FlatList
                      data={this.state.get_products}
                      renderItem={( {item, index} ) => {
                        // console.warn( `id=${item.id} index=${index}` );
                        return (<ProductItem ref='ProductItem' 
                          key={index} 
                          row={item}
                          index={index}
                          handleOfCheckedProduct={ this._handleSelectProductByChecked }
                        />)
                      }}
                      keyExtractor={(item, index) => item.title}
                      refreshing={this.state.refreshing}
                    />
                  </View>

                  { this.state.get_products.length ? 
                  (<Animatable.View animation="flipInX" style={{  flex: 1, flexDirection: 'row', alignContent:'center', alignItems: 'center', marginTop: 10 }}>
                    <Button 
                      style={[styles.touchBtnSubmit, {flex: 1, textAlign: 'center', marginRight: 5}]}
                      onPress = { () => this.onHandleRefreshProducts() }
                    >
                      <Text style={[styles.btnButtonSubmit, { width: 135,textAlign: 'center' }]}>Referesh</Text>
                    </Button>
                    <Button 
                      style={[styles.touchBtnSubmit, {flex: 1, textAlign: 'center', marginLeft: 5, backgroundColor: (this.state.btnAddToCartDisable ? '#ff8183' : '#ed3134') }]}
                      onPress = { () => this.onHandleAddToCart() }
                      disabled={ this.state.btnAddToCartDisable }
                    >
                      <Text style={[styles.btnButtonSubmit, { width: 135,textAlign: 'center' }]}>Add to Cart</Text>
                    </Button>
                  </Animatable.View>)
                  : null }
                  
                  <Animatable.View animation="flipInX">
                    <TextInput
                      style={ [styles.fieldTextarea, !this.state.validate_of_details ? styles.err : null] } 
                      multiline={true}
                      numberOfLines={6}
                      minHeight={150}
                      maxHeight={150}
                      underlineColorAndroid='transparent'
                      placeholder= 'Your Requirement'
                      placeholderTextColor= '#ffffff'
                      // value={ this.state.details }
                      onChangeText = { (text) => this.setState({ details: text }) }
                    />
                  </Animatable.View>

                  <View>
                    <Text style={ !this.state.validate_of_details ? customStyles.usama:null }>{ this.state.validate_of_details_msg }</Text>
                  </View>

                  {/* List of Upload Your Queries */}
                  { this.state.selection_files.map ( ( value, index ) => {
                        // console.warn( `id=${item} index=${index}` );
                        return <ProductFileItem 
                          key={index}
                          index={index}
                          row={ value }
                          handleOfDelFile={ this.hanldeOfTempDelFiles }
                          handleOfModalImage={ this._display_of_modal_image }
                        />
                  })}

                  {/* Upload Your Query */}
                  <Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 0 }}>
                    <View style={styles.touchBtnSubmit}>
                      <TouchableOpacity onPress = {  this.hanldeOfUploadYourQuery.bind(this) } >
                        <Text style={styles.btnButtonSubmit}>Attach File</Text>      
                      </TouchableOpacity>
                    </View>
                  </Animatable.View>

                  <Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center' }}>
                    <TouchableOpacity 
                      style={styles.touchBtnSubmit}
                      onPress = { () => this.onHandleReviewSelect(navigate) }
                    >
                      <Text style={styles.btnButtonSubmit}>Preview</Text>
                    </TouchableOpacity>
                  </Animatable.View>
              </View>
            </Content>

            <Footer style={{ backgroundColor:'transparent' }}>
              <FooterCopyRight />
            </Footer>

            <Modal
							transparent={ true }
							visible={this.state.isMessageModal}
							onRequestClose={ () => console.warn('this is') }
						>
							<View style={ customStyles.modalBody }>
								<View style={ customStyles.modalContainer }>
									<View style={ customStyles.modalBox }>
										<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
												this.setState({
													isMessageModal: false
												})
											}}>
												<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										<Text style={ customStyles.modalTextMessage }>
											{ this.state.isMessageModalText }
										</Text>
									</View>
								</View>
							</View>
						</Modal>

            <Modal
              transparent={ true }
              visible={this.state.isMessageModal1}
              onRequestClose={ () => console.warn('this is') }
            >
              <View style={ customStyles.modalBody }>
                <View style={ customStyles.modalContainer }>
                  <View style={ customStyles.modalBox }>
                    <TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
                        this.setState({
                          isMessageModal1: false
                        })
                      }}>
                    </TouchableOpacity>
                    <Text style={ customStyles.modalTextMessage }>
                      { this.state.isMessageModalText1 }
                    </Text>
                  </View>
                </View>
              </View>
            </Modal>

            <Modal
							transparent={ true }
							visible={this.state.isImageModal}
							onRequestClose={ () => console.warn('this is') }
						>
							<View style={ customStyles.modalBody }>
								<View style={ customStyles.modalContainer }>
									<View style={ customStyles.modalBox }>
										<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
												this.setState({
													isImageModal: false
												})
											}}>
											<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										 <Image
                        resizeMode='cover'
                        style={{borderRadius:10, width: 300, height: 180}}
                        source={{uri: this.state.isImageSourceURL }}
                      />
									</View>
								</View>
							</View>
						</Modal>

          </Container>
      </ImageBackground>
      );
   };
}