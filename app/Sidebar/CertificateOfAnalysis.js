import React from 'react';
import { Text, View,  Image, ImageBackground,  StyleSheet, TextInput, TouchableOpacity,  AsyncStorage, Modal, KeyboardAvoidingView } from 'react-native';
import { Header, Content, Footer, Container, Left, Body, Right, Title, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker';

import * as Animatable from 'react-native-animatable';
import FCM, {FCMEvent} from "react-native-fcm";

import Config from './../Services/Config';
import Validation from './../Services/Validation';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

import customStyles from './../styles/Custom';

export default class CertificateOfAnalysis extends React.Component {

  	constructor(props)
	{
		super(props);

		this.btnFormSubmit = this.btnFormSubmit.bind(this);

		this.state = {

			user_id: 0,
			
			validate_of_product_name: true,
			validate_of_product_name_msg: '',
			product_name: '',

			validate_of_brand_name: true,
			validate_of_brand_name_msg: '',
			brand_name: '',

			validate_of_cat_no: true,
			validate_of_cat_no_msg: '',
			cat_no: '',

			validate_of_lot_no: true,
			validate_of_lot_no_msg: '',
			lot_no: '',

			validate_of_expiry_date: true,
			validate_of_expiry_date_msg: '',
			expiry_date: '',

			isLoading: false,

			isMessageModal: false,
			isMessageModalText: ''
		};

		AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));

		FCM.on(FCMEvent.Notification, (notif) => {
           this.setState({ isMessageModal: true, isMessageModalText: notif.body })
        });
	};

	_form_validation = () => {

		let product_name 		= this.state.product_name;
		let brand_name 			= this.state.brand_name;
		let cat_no 				= this.state.cat_no;
		let lot_no 				= this.state.lot_no;
		let expiry_date 		= this.state.expiry_date;
		
		this.setState({ validate_of_product_name: ((product_name != "" ) ? true : false) });
		this.setState({ 
			validate_of_product_name_msg: ((product_name == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_brand_name: ((brand_name != "" ) ? true : false) });
		this.setState({ 
			validate_of_brand_name_msg: ((brand_name == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_cat_no: ((cat_no != "" ) ? true : false) });
		this.setState({ 
			validate_of_cat_no_msg: ((cat_no == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_lot_no: ((lot_no != "" ) ? true : false) });
		this.setState({ 
			validate_of_lot_no_msg: ((lot_no == "") ? 'This field is required' : '') 
		});

		this.setState({ validate_of_expiry_date: ((expiry_date != "" ) ? true : false) });
		this.setState({ 
			validate_of_expiry_date_msg: ((expiry_date == "") ? 'This field is required' : '') 
		});
	}

	btnFormSubmit = () => {
		
		this._form_validation();

		let is_validate = ( 
			( this.state.validate_of_product_name && this.state.product_name) &&
			( this.state.validate_of_brand_name && this.state.brand_name) &&
			( this.state.validate_of_cat_no && this.state.cat_no) &&
			( this.state.validate_of_lot_no && this.state.lot_no) &&
			( this.state.validate_of_expiry_date && this.state.expiry_date)
		);
		
		if( is_validate )
		{
			let postdata = {
				user_id: this.state.user_id,
				product_name: this.state.product_name,
				brand_name: this.state.brand_name,
				cat_no: this.state.cat_no,
				lot_no: this.state.lot_no,
				expiry_date: this.state.expiry_date
            }

			this.ajaxPostFormSubmit( postdata );
		}
	};

	ajaxPostFormSubmit( postdata )
	{
		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/certificate-analysis", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{
				this.setState({
					product_name: '',
					brand_name: '',
					cat_no: '',
					lot_no: '',
					expiry_date: ''
				});
			}

			this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
			});
		})
		.catch( (error) => console.warn(error) )
		.done();
	}
	
	render()
	{
		return (
			<KeyboardAvoidingView  behavior="padding" enabled style={{
				flex: 1
			  }}>
			<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

				<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

				<Container style={{ backgroundColor:'transparent'}}>
					<Header style={{ backgroundColor:'#ed3134' }} >
						<Left>
							<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
								<Image 
									source={ require('./../img/icon/menu.png') }
									style={{ height:20, width:20 }}
								/>
							</TouchableOpacity>
						</Left>
						<Body>
							<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Certificate of Analysis</Title>
						</Body>
						<Right>
			                <TouchableOpacity>
			                  <Button transparent onPress={() => this.props.navigation.navigate('Support Hub')}>
			                      <Image 
			                      source={ require('./../img/icon/arrow.png') }
			                      style={{ height:12, width:22 }}
			                    />
			                  </Button>
			                </TouchableOpacity>
			            </Right>
					</Header>
					<Content contentContainerStyle={ styles.bodyContainer }>

						<View style={styles.bodySubContainer}>
								
								<Animatable.View animation="flipInX">
									<TextInput 
										style={ [styles.inputField, !this.state.validate_of_product_name ? styles.err:null] } 
										underlineColorAndroid='transparent'
										placeholder= 'Product Name'
										placeholderTextColor= '#ffffff'
										multiline={true}
										// value={ this.state.product_name }
										onChangeText = { (text) => this.setState({ product_name: text }) }
										minLength={5}
										maxLength={20}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_product_name ? customStyles.usama:null }>{ this.state.validate_of_product_name_msg }</Text>
								</View>

								<Animatable.View animation="flipInX">
									<TextInput 
										style={ [styles.inputField, !this.state.validate_of_brand_name ? styles.err:null] } 
										underlineColorAndroid='transparent'
										placeholder= 'Brand Name'
										multiline={true}
										placeholderTextColor= '#ffffff'
										// value={ this.state.brand_name }
										onChangeText = { (text) => this.setState({ brand_name: text }) }
										minLength={5}
										maxLength={20}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_brand_name ? customStyles.usama:null }>{ this.state.validate_of_brand_name_msg }</Text>
								</View>

								<Animatable.View animation="flipInX">
									<TextInput 
										style={ [styles.inputField, !this.state.validate_of_cat_no ? styles.err:null] } 
										// underlineColorAndroid='transparent'
										placeholder= 'Cat No'
										multiline={true}
										placeholderTextColor= '#ffffff'
										// value={ this.state.cat_no }
										onChangeText = { (text) => this.setState({ cat_no: text }) }
										minLength={5}
										maxLength={20}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_cat_no ? customStyles.usama:null }>{ this.state.validate_of_cat_no_msg }</Text>
								</View>

								<Animatable.View animation="flipInX">
									<TextInput 
										style={ [styles.inputField, !this.state.validate_of_lot_no ? styles.err:null] } 
										// underlineColorAndroid='transparent'
										placeholder= 'Lot No'
										multiline={true}
										placeholderTextColor= '#ffffff'
										// value={ this.state.lot_no }
										onChangeText = { (text) => this.setState({ lot_no: text }) }
										minLength={5}
										maxLength={20}
									/>
									
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_lot_no ? customStyles.usama:null }>{ this.state.validate_of_lot_no_msg }</Text>
								</View>

								<Animatable.View animation="flipInX">
									<DatePicker
										style={ [styles.inputDateField, !this.state.validate_of_expiry_date ? styles.err:null] } 
										key={1}
										date={this.state.expiry_date}
										mode="date"
										value={ this.state.expiry_date}
										placeholder="Expiry"
										format="YYYY-MM-DD"
										confirmBtnText="Confirm"
										cancelBtnText="Cancel"
										iconSource={ require('./../img/icon/calendar.png') }
										customStyles={{
											dateIcon: {
												position: 'absolute',
												right: 0,
												top: 11,
												height:18,
												width:16
											},
											dateInput: {
												borderWidth: 0,
												alignItems: 'flex-start',
											},
											placeholderText: {
												fontSize: 16,
												color: '#fff',
											},
											dateText: {
												fontSize: 13,
												color: '#fff'
											}
										}}
										onDateChange={(date) => {this.setState({expiry_date: date})}}
									/>
								</Animatable.View>
								<View>
									<Text style={ !this.state.validate_of_expiry_date ? customStyles.usama:null }>{ this.state.validate_of_expiry_date_msg }</Text>
								</View>

						</View>

						<Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 40 }}>
							<Button 
								style={styles.touchBtnSubmit}
								onPress = {() => this.btnFormSubmit()}
							>
								<Text style={styles.btnButtonSubmit}>Submit</Text>
							</Button>
						</Animatable.View>
					</Content>

					<Footer style={{ backgroundColor:'transparent' }}>
						<FooterCopyRight />
					</Footer>


					<Modal
						transparent={ true }
						visible={this.state.isMessageModal}
						onRequestClose={ () => console.warn('this is') }
					>
						<View style={ customStyles.modalBody }>
							<View style={ customStyles.modalContainer }>
								<View style={ customStyles.modalBox }>
									<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
											this.setState({
												isMessageModal: false
											})
										}}>
											<Icon name="times-circle" size={18} color="white" />
									</TouchableOpacity>
									<Text style={ customStyles.modalTextMessage }>
										{ this.state.isMessageModalText }
									</Text>
								</View>
							</View>
						</View>
					</Modal>

				</Container>
		</ImageBackground>
		</KeyboardAvoidingView>
		);
	} 
}

const styles = StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20,
	},

	inputField: {
		width: 300,
		backgroundColor: '#5a5a58',
		borderRadius: 25,
		paddingHorizontal: 16,
		paddingTop:10,
		fontSize: 16,
		color: '#ffffff',
		marginVertical: 5,
		height:40,
		alignSelf: 'center'
	},

    inputDateField: {
        width: 300,
		backgroundColor: '#5a5a58',
		borderRadius: 25,
		paddingHorizontal: 20,
		paddingVertical:4,
		marginVertical: 5
    },

	fieldTextarea: {
		width: 300,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		fontSize: 16,
		color: '#ffffff',
		paddingHorizontal: 20,
		paddingVertical:12,
		marginVertical: 5
    },
    
    dateDropDownPicker: {
        color: '#ffffff',
        // borderRadius: 14,
        borderWidth: 1,
        backgroundColor: '#5a5a58',
        width: 300,
        borderRadius: 25,
        marginVertical: 5
    },

	touchBtnSubmit: {
		width: 300,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 5,
		paddingVertical: 12
	},

	btnButtonSubmit: {
		width: 300,
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},
    
    err: {
		borderWidth:1,
		borderColor: 'red'
	}
});