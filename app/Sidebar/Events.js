import React from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity, ListView } from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import Config from './../Services/Config';

import * as Animatable from 'react-native-animatable';

// Components
import EventItem from './../components/Event/EventItem';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

// Assets
import StyleGridList from './../styles/GridList';

export default class Event extends React.Component {
  
	static navigationOptions = {
		drawerIcon: (
		<Image 
			source={ require('./../img/icon/id-event-icon.png') }
			style={{ height:20, width:20 }}
		/>
		)
	}

  	constructor()
	{
		super();
		
		var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

		this.state = {
			dataSource: ds,

			isLoading: false
		};
	};

	componentDidMount()
	{

		this.setState({ isLoading: true });
		
		let api_url = (Config.base_url() + "/api/data/events");

		fetch( api_url, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });

			var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

			this.setState({
				dataSource: ds.cloneWithRows(res.data)
			});

		})
		.catch(error => console.log(error))
		.done();
	};

	render()
	{
		const { navigate } = this.props.navigation;
		
		return (
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />
			
			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Events</Title>
					</Body>
					<Right>
	                <TouchableOpacity>
	                  <Button transparent onPress={() => navigate('Home') }>
	                      <Image 
	                      source={ require('./../img/icon/arrow.png') }
	                      style={{ height:12, width:22 }}
	                    />
	                  </Button>
	                </TouchableOpacity>
	              </Right>
				</Header>
				<Content>

					<View style={ StyleGridList.container }>
						<ListView
							dataSource={this.state.dataSource}
							renderRow={  (rowData) => <EventItem event_item={ rowData } navigate={ navigate } key={rowData.id}  /> }
						/>
					</View>

				</Content>
				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>
			</Container>
		</ImageBackground>
		);
	} 
}