import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Text, View, Image, ImageBackground, TextInput, TouchableOpacity, StyleSheet, Modal, Alert } from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title} from 'native-base';

import { CustomPicker } from 'react-native-custom-picker';
// Styles & Formatting
import Config from './../Services/Config';
import customStyles from './../styles/Custom';

// Components
import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class QuickInquiry extends React.Component {
  
	static navigationOptions = {
		drawerIcon: (
		<Image 
			source={ require('./../img/icon/contact-us.png') }
			style={{ height:20, width:20 }}
		/>
		)
	}

  	constructor()
	{
		super();

		this.btnFormSubmit = this.btnFormSubmit.bind(this);

		this.state = {
			validate_of_name: true,
			name: '',

			validate_of_email: true,
			email: '',

			validate_of_phone: true,
			phone: '',

			validate_of_message: true,
			message: '',

			isMessageModal: false,
			isMessageModalText: ''
		};
	};

	get_validate(value, field)
	{
		if( field == 'name' )
		{
			this.setState({ validate_of_name: ((value=="") ? false : true) });
			this.setState({name: value});
		}

		else if( field == 'email' )
		{
			var email = /\S+@\S+\.\S+/;

			if( value == '' )
			{
				this.setState({
					validate_of_email: false
				});
			}	
			else
			{
				this.setState({
					validate_of_email: true
				});
			}

			this.setState({email: value});
		}

		else if( field == 'phone' )
		{
			this.setState({ validate_of_phone: ((value=="") ? false : true) });
			this.setState({phone: value});
		}
		
		else if( field == 'message' )
		{
			this.setState({ validate_of_message: ((value=="") ? false : true) });
			this.setState({message: value});	
		}
	}

	btnFormSubmit = () => {
		
		let is_validate = ( 
			(this.state.validate_of_name && this.state.name ) &&
			( this.state.validate_of_email && this.state.email) &&
			(this.state.validate_of_phone && this.state.phone) &&
			(this.state.validate_of_message && this.state.message)
		);
		
		if( is_validate )
		{
			let postdata = {
				user_id: 5,
				name: this.state.name,
				phone: this.state.phone,
				email: this.state.email,
				message: this.state.message
			}

			fetch( Config.base_url() + "/api/user/contactus", {
				method: "POST",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(postdata)
			})
			.then((response) => response.json() )
			.then((res) => {

				if( res.status )
				{
					this.setState({
						name: '',
						email: '',
						phone: '',
						message: '',

						isMessageModal: true,
						isMessageModalText: res.message
					});
				}
				else
				{
					this.setState({
						validate_of_name: false,
						validate_of_email: false,
						validate_of_phone: false,
						validate_of_messsage: false
					});
				};
			})
			.catch(function(error){
				console.warn(error);
			})
			.done();
		}
		else
		{
			this.setState({
				validate_of_name: (this.state.name ? true : false),
				validate_of_email: (this.state.email ? true : false),
				validate_of_phone: (this.state.phone ? true : false),
				validate_of_message: (this.state.message ? true : false)
			});
		}
	};

	render()
	{
		const {goBack} = this.props.navigation;
		const options = ['One', 'Two', 'Three', 'Four', 'Five']

		return (
			<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >
			
				<Container style={{ backgroundColor:'transparent'}}>
					<Header style={{ backgroundColor:'#ed3134' }} >
						<Left>
							<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
								<Image 
									source={ require('./../img/icon/menu.png') }
									style={{ height:20, width:20 }}
								/>
							</TouchableOpacity>
						</Left>
						<Body>
							<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center' }} >Quick Quotes/Enquiry</Title>
						</Body>
						<Right>
							<TouchableOpacity onPress={() => goBack() }>
								<Image 
									source={ require('./../img/icon/arrow.png') }
									style={{ height:20, width:30 }}
								/>
							</TouchableOpacity>
						</Right>
					</Header>
					<Content contentContainerStyle={ styles.bodyContainer }>



						<View style={styles.bodySubContainer}>

								<Text style={{ fontSize:20, marginTop:5, marginBottom:10, fontWeight:'bold', color:'#ee3135' }}>Search Product</Text>

								<CustomPicker
						          options={options}
						          onValueChange={value => {
						            Alert.alert('Selected Item', value || 'No item were selected!')
						          }}
						        />

	                            
								
						</View>

						<View style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 35 }}>
							<TouchableOpacity 
								style={styles.touchBtnSubmit}
								onPress = {() => this.btnFormSubmit()}
							>
								<Text style={styles.btnButtonSubmit}>Submit</Text>
							</TouchableOpacity>

							<TouchableOpacity 
								style={styles.touchBtnSubmit}
								onPress = {() => this.btnFormSubmit()}
							>
								<Text style={styles.btnButtonSubmit}>Submit</Text>
							</TouchableOpacity>
						</View>

						<Modal
							transparent={ true }
							visible={this.state.isMessageModal}
							onRequestClose={ () => console.warn('this is') }
						>
							<View style={ customStyles.modalBody }>
								<View style={ customStyles.modalContainer }>
									<View style={ customStyles.modalBox }>
										<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
												this.setState({
													isMessageModal: false
												})
											}}>
												<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										<Text style={ customStyles.modalTextMessage }>
											{ this.state.isMessageModalText }
										</Text>
									</View>
								</View>
							</View>
						</Modal>
					</Content>

					<Footer style={{ backgroundColor:'transparent' }}>
						<FooterCopyRight />
					</Footer>

				</Container>
		</ImageBackground>
		);
	} 
}

const styles = StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20,
	},

	inputField: {
		width: 300,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingHorizontal: 16,
		fontSize: 16,
		color: '#ffffff',
		marginVertical: 10
	},

    inputDateField: {
        width: 300,
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingHorizontal: 20,
		paddingVertical:4,
		marginVertical: 10
    },

	fieldTextarea: {
		width: 300,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		fontSize: 16,
		color: '#ffffff',
		paddingHorizontal: 20,
		paddingVertical:12,
		marginVertical: 10
    },
    
    dateDropDownPicker: {
        color: '#ffffff',
        // borderRadius: 14,
        borderWidth: 1,
        backgroundColor: '#5a5a58',
        width: 300,
        borderRadius: 25,
        marginVertical: 10
    },

	touchBtnSubmit: {
		width: 300,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 10,
		paddingVertical: 12
	},

	btnButtonSubmit: {
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},
    
    err: {
		borderWidth:1,
		borderColor: 'red'
	},

});