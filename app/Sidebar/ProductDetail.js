import React from 'react';
import {Text, View, Image, ImageBackground, TouchableOpacity, StyleSheet, ScrollView, Modal} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button } from 'native-base';

import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
// import RNFetchBlob from 'rn-fetch-blob';
import PDFView from 'react-native-view-pdf';

import * as Animatable from 'react-native-animatable';

import Config from './../Services/Config';

import SingleGrid from './../styles/SingleGridDetail';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

import customStyles from './../styles/Custom';

export default class ProductDetail extends React.Component {

  	constructor(props)
	{
		super(props);

		this.state = {
			single_detail: null,
			single_detail_title: 'Respective Brand',

			isLoading: false,

			isMessageModal: false,
			isMessageModalText: ''
		};
	};

	getSinglePage( id )
	{
		this.setState({ isLoading: true });

		let api_url = (Config.base_url() + "/api/data/single-product-detail?product_id=" + id );
		
		fetch( api_url, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });

			let data = res.data;
			this.setState({
				single_detail: data,
				single_detail_title: data.title
			});

		})
		.catch(error => console.warn(error))
		.done();
	}

	componentDidMount()
	{
		const {params} = this.props.navigation.state;
		
		this.getSinglePage( params.product_id );
	};

	getFileDownload( absolute_file )
	{
		this.props.navigation.navigate('PDFViewComponent', { url: Config.base_url() + absolute_file})
		// this.setState({ isLoading: true });

		// let carbon      	= new Date();
		// let download_url 	= (Config.base_url() + absolute_file );
		// let ext       		= this.extention(download_url);
		// ext = "."+ext[0];

		// const { config, fs } = RNFetchBlob;
		
		// let PictureDir = fs.dirs.PictureDir
		// let options = {
		// 		fileCache: true,
		// 		addAndroidDownloads : {
		// 		useDownloadManager : true,
		// 		notification : true,
		// 		path:  PictureDir + "/image_"+Math.floor(carbon.getTime() + carbon.getSeconds() / 2) + ext,
		// 		description : 'Image'
		// 	}
		// }

		// config(options).fetch('GET', download_url).then((res) => {

		// 	this.setState({ isLoading: false });

		// 	this.setState({
		// 		isMessageModal: true,
		// 		isMessageModalText: 'Downloaded Successfully'
		// 	});
		// });
	}

	extention(filename){
		return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
	}

	render()
	{
		const is_product = (this.state.single_detail);
		const { navigate } = this.props.navigation;
		// const { config, fs } = RNFetchBlob;

		return (
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={() => this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >{ this.state.single_detail_title }</Title>
					</Body>
					<Right>
		                <TouchableOpacity>
		                  <Button transparent onPress={() => this.props.navigation.navigate('ProductList') }>
		                      <Image 
		                      source={ require('./../img/icon/arrow.png') }
		                      style={{ height:12, width:22 }}
		                    />
		                  </Button>
		                </TouchableOpacity>
		              </Right>
				</Header>

		          <Content>

						<View style={  SingleGrid.container }>
							{ 
							(( is_product )
							?
							(

							<View style={ styles.bodyContainer }>
								
							<ScrollView>	
								<Animatable.View animation="flipInX" style={ styles.bodySubContainer }>
									<Text style={ styles.heading1 }>{this.state.single_detail.title}</Text>
									<View style={{ borderBottomColor: '#ed3134', borderBottomWidth: 2, width: 90, marginVertical: 15,  alignItems: 'center' }} ></View>
									<View>
										<Text style={ styles.paragraph }>
											{this.state.single_detail.description}
										</Text>
									</View>
								</Animatable.View>
							</ScrollView>

							<Animatable.View animation="flipInX" style={{ alignContent:'center', alignItems: 'center', marginTop: 20 }}>
								<TouchableOpacity 
									style={{ width: 300, backgroundColor: '#ed3134', borderRadius: 25, marginVertical: 45, paddingVertical: 12 }}
									onPress={ () => this.getFileDownload( this.state.single_detail.file_chunks.file_url ) }
								>
									<View>
										<Text style={{ fontSize: 16, fontWeight: '500', color:'#ffffff', textAlign: 'center', right: 5 }}>Open PDF</Text>
										<Image 
											source={ require('./../img/icon/download-icon.png') }
											style={{ height:18, width:18, position:'absolute', right:75, top:2 }}
										/>
									</View>
								</TouchableOpacity>
							</Animatable.View>

							</View>)
							: 
							null
							)
							}
						</View>
					</Content>
				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>

				<Modal
					transparent={ true }
					visible={this.state.isMessageModal}
					onRequestClose={ () => console.warn('this is') }
				>
					<View style={ customStyles.modalBody }>
						<View style={ customStyles.modalContainer }>
							<View style={ customStyles.modalBox }>
								<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
										this.setState({
											isMessageModal: false
										})
									}}>
										<Icon name="times-circle" size={18} color="white" />
								</TouchableOpacity>
								<Text style={ customStyles.modalTextMessage }>
									{ this.state.isMessageModalText }
								</Text>
							</View>
						</View>
					</View>
				</Modal>
			</Container>
		</ImageBackground>
		);
	} 
}

const styles = StyleSheet.create({

  bodyContainer: {
    flex:1,
    height:480
  },

  bodySubContainer:{
    flex:1,
    alignItems:'center',
    justifyContent: 'center',
    padding:20
  },

  heading1:{
    alignItems:'center',
    justifyContent: 'center',
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#ed3135',
    textAlign: 'center',
    
  },

  paragraph:{
    alignItems:'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Roboto-bold'
  }

});