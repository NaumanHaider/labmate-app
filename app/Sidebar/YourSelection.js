import React from 'react';
import { Text, View, Image, ImageBackground, TextInput, TouchableOpacity, AsyncStorage, FlatList, Modal} from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import {Select, Option} from "react-native-chooser";
import Spinner from 'react-native-loading-spinner-overlay';

import * as Animatable from 'react-native-animatable';
import FCM, {FCMEvent} from "react-native-fcm";

import Config from './../Services/Config';
import Validation from './../Services/Validation';
import ArrayHelper from './../Services/ArrayHelper';

// Styles & Formatting
import styles from './../styles/QuickQuotos';
import customStyles from './../styles/Custom';

// Components
import SelectionProductItem from './../components/QuickQuote/SelectionProductItem';
import SelectionFileItem from './../components/QuickQuote/SelectionFileItem';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

export default class YourSelection extends React.Component
{

  constructor ()
  {
    super();

    this.state = {
      user_id: 0,
      is_modified: false, 
      is_saved: false, 
      
      details: '',
      validate_of_details: true,
      validate_of_details_msg: '',

      //Loading
      isLoading: false,
      refreshing: false,

      //Modals
      isMessageModal: false,
      isMessageModalText: '',

      isImageModal: false,
      isImageSourceURL: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg',

      quick_quotes: '',
      selection_products: [],
      selected_files: []
    }

    this._delete_product_id = this._delete_product_id.bind(this);
    this._delete_file_id = this._delete_file_id.bind(this);
    this._display_of_modal_image = this._display_of_modal_image.bind(this);
    
    AsyncStorage.getItem('user_id').then((value) => this.setState({ 'user_id': value }));

    FCM.on(FCMEvent.Notification, (notif) => {
      this.setState({ isMessageModal: true, isMessageModalText: notif.body })
    });
  }

  componentDidMount()
	{
    AsyncStorage.getItem('user_id').then((value) => this.getUserSelections( value ) );
  };

  getUserSelections = ( user_id ) => {
    
    this.setState({ isLoading: true });
    
    let api_url_by_select_products = (Config.base_url() + "/api/data/selection-products?user_id=" + user_id );

		fetch( api_url_by_select_products, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

      this.setState({ isLoading: false });

      let status    = res.status;
      let is_data   = Object.keys(res.data).length;

      if( status && is_data )
      {
        let quick_quotes  = res.data.quick_quotes;
        let is_button     = ((quick_quotes.description ||  quick_quotes.products.length || quick_quotes.files !== undefined) ? true : false);

        this.setState({ 
          quick_quotes: quick_quotes,
          is_modified: is_button,
          is_saved: is_button
        });

        this.setState({ 
          details: quick_quotes.description
        });

        if( quick_quotes.products.length || quick_quotes.files )
        {
          let selected_products = this.state.quick_quotes.products || [];
          let selected_files = this.state.quick_quotes.files || [];

          this.setState({ 
            selection_products: selected_products,
            selected_files: selected_files
          });
        }
      }
              
		})
		.catch(error => console.log(error))
    .done();
  }

  hanldeOfFormValidation = () => {

    let details 		          = this.state.details;

    if( this.state.selection_products.length < 1 )
    {
      this.setState({ validate_of_details: ((details != "" && !Validation.isNotAllowSpecialCharacters(details) ) ? true : false) });
      this.setState({ validate_of_details_msg: (
        (details == "" ) ? 
        'This field is required' : 
        ( Validation.isNotAllowSpecialCharacters(details) ? 'This format is invalid' : '')
      )});
    }
  }

  hanldeOfAjaxRemoteFormSubmit = ( postdata ) => {
  
    this.setState({ isLoading: true });

    fetch( Config.base_url() + "/api/user/final-quick-quote-enquiry", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {
      
      this.setState({ isLoading: false });
      
      // Reset
      this.setState({
        details: '',
        selection_products: [],
        selected_files: []
      });

      this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
      });
      
      this.handleOfButton();
		})
    .done();
  }

  handleOfQuerySubmit() {
    
    this.hanldeOfFormValidation();

    let is_validate           = (this.state.details != "");
    let is_products           = this.state.selection_products.length;
    
    let postdata = {
      user_id: this.state.user_id,
      description: this.state.details
    } 

    if( is_validate )
    {
      // console.warn( 'only content');
      this.hanldeOfAjaxRemoteFormSubmit( postdata );
    }

    if( is_products )
    {
      // console.warn( 'only producfts');
      this.hanldeOfAjaxRemoteFormSubmit( postdata );
    }

    if( is_validate == false && is_products == false )
    {
      this.setState({
        isMessageModal: true,
        isMessageModalText: `Please fill any one: 1=Product, 2=Form`
      });
    }

  }

  _delete_product_id( id, index ) {
    
    // console.warn ( `ID: ${id}, index: ${index}` );
    
    let getProducts   = this.state.selection_products;

    if( getProducts[index] && getProducts[index].id )
      this._deleteProductOnRemote( getProducts[index].id );

    getProducts.splice(index, 1);

    if( getProducts.length < 1)
        getProducts = []

    this.setState({
      selection_products: getProducts
    });

    this.handleOfButton();
  }

  _deleteProductOnRemote = ( id ) => {

    this.setState({ isLoading: true });
    
    let postdata = {
      user_id: this.state.user_id,
      product_id: id
    };
    
		fetch( Config.base_url() + "/api/data/delete-selection-product", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {
      
      this.setState({ isLoading: false });
      
      this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
      });
      
		})
    .done();
  }

  /***
   * Handle of User Files
  */

 _display_of_modal_image = ( source ) => {
    
    let image_source = Config.base_url() + source;

    this.setState({
      isImageModal: true,
      isImageSourceURL: image_source
    });
  }

  _delete_file_id = ( id, index ) => {
    
    // console.warn ( `ID: ${id}, index: ${index}` );
    
    let getFiles   = this.state.selected_files;

    if( getFiles[index] && getFiles[index].id )
      this._deleteFileOnRemote( getFiles[index].source_name );

    getFiles.splice(index, 1);

    if( getFiles.length < 1)
        getFiles = []

    this.setState({
      selected_files: getFiles
    });

    this.handleOfButton();
  }

  _deleteFileOnRemote = ( id ) => {

    this.setState({ isLoading: true });
    
    let postdata = {
      user_id: this.state.user_id,
      source: id
    };
    
		fetch( Config.base_url() + "/api/data/delete-selection-file", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {
      
      this.setState({ isLoading: false });
      
      this.setState({ 
				isMessageModal: true,
				isMessageModalText: res.message
      });   
		})
    .done();    
  }

  handleOfButton = () => {

    let is_products = this.state.selection_products;
    let is_files    = this.state.selected_files;
    let is_desc     = this.state.details || '';

    /*console.warn(`
      is_products: ${is_products.length}, 
      is_files: ${is_files.length}, 
      is_desc: ${is_desc}
    `);*/

    if( is_products.length == 0 && is_files.length == 0 && is_desc == '' )
    {
      this.setState({
        is_modified: false,
        is_saved: false
      });
    }
  }

  render()
  {
    const { navigate } = this.props.navigation;

    return (
      <ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >
        
         <Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

          <Container style={{ backgroundColor:'transparent'}}>
            <Header style={{ backgroundColor:'#ed3134' }} >
              <Left>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
                  <Image 
                    source={ require('./../img/icon/menu.png') }
                    style={{ height:20, width:20 }}
                  />
                </TouchableOpacity>
              </Left>

              <Body>
                <Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Your Selection</Title>
              </Body>
              <Right>
                  <TouchableOpacity>
                    <Button transparent onPress={() => navigate('Request for Quotation') }>
                        <Image 
                        source={ require('./../img/icon/arrow.png') }
                        style={{ height:12, width:22 }}
                      />
                    </Button>
                  </TouchableOpacity>
              </Right>
            </Header>
            <Content contentContainerStyle={ styles.bodyContainer }>

              <View style={styles.bodySubContainer}>

                  <FlatList
                    data={this.state.selection_products}
                    renderItem={( {item, index} ) => {
                      // console.warn( `id=${item.id} index=${index}` );
                      return (<SelectionProductItem ref='SelectionProductItem' 
                        key={index} 
                        row={item}
                        index={index}
                        handleOfDelProduct={ this._delete_product_id }
                      />)
                    }}
                    keyExtractor={(item, index) => item.title}
                    refreshing={true}
                  />

                  <Animatable.View animation="flipInX">
                    <TextInput
                      style={ [styles.fieldTextarea, !this.state.validate_of_details ? styles.err : null] } 
                      multiline={true}
                      numberOfLines={6}
                      minHeight={150}
                      maxHeight={150}
                      underlineColorAndroid='transparent'
                      placeholder= 'Your Requirement'
                      placeholderTextColor= '#ffffff'
                      // value={ this.state.details }
                      onChangeText = { (text) => this.setState({ details: text }) }
                    />
                  </Animatable.View>
                  <View>
                    <Text style={ !this.state.validate_of_details ? customStyles.usama:null }>{ this.state.validate_of_details_msg }</Text>
                  </View>

                  <FlatList
                    data={this.state.selected_files}
                    renderItem={( {item, index} ) => {
                      return (<SelectionFileItem ref='SelectionFileItem' 
                        key={index} 
                        row={item}
                        index={index}
                        handleOfModalImage={ this._display_of_modal_image }
                        handleOfDelFile={ this._delete_file_id }
                      />)
                    }}
                    keyExtractor={(item, index) => item.source}
                    refreshing={this.state.refreshing}
                  />
                  
                  { this.state.is_modified ?
                  <Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center', marginTop: 35 }}>
                    <TouchableOpacity onPress={() => navigate('Request for Quotation') }
                      style={styles.touchBtnSubmit}
                    >
                      <Text style={styles.btnButtonSubmit}>Add More Items</Text>
                    </TouchableOpacity>
                  </Animatable.View>
                  : null }

                  { this.state.is_saved ?
                  <Animatable.View animation="flipInX" style={{  flex: 1, alignContent:'center', alignItems: 'center' }}>
                    <TouchableOpacity 
                      style={styles.touchBtnSubmit}
                      onPress = { () => this.handleOfQuerySubmit() }
                    >
                      <Text style={styles.btnButtonSubmit}>Submit</Text>
                    </TouchableOpacity>
                  </Animatable.View>
                  : null }
                
              </View>
              
            </Content>
            <Footer style={{ backgroundColor:'transparent' }}>
              <FooterCopyRight />
            </Footer>

            <Modal
							transparent={ true }
							visible={this.state.isMessageModal}
							onRequestClose={ () => console.warn('this is') }
						>
							<View style={ customStyles.modalBody }>
								<View style={ customStyles.modalContainer }>
									<View style={ customStyles.modalBox }>
										<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
												this.setState({
													isMessageModal: false
												})
											}}>
												<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										<Text style={ customStyles.modalTextMessage }>
											{ this.state.isMessageModalText }
										</Text>
									</View>
								</View>
							</View>
						</Modal>

            <Modal
							transparent={ true }
							visible={this.state.isImageModal}
							onRequestClose={ () => console.warn('this is') }
						>
							<View style={ customStyles.modalBody }>
								<View style={ customStyles.modalContainer }>
									<View style={ customStyles.modalBox }>
										<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
												this.setState({
													isImageModal: false
												})
											}}>
											<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										 <Image
                        style={{borderRadius:10, width: 300, height: 180}}
                        source={{uri: this.state.isImageSourceURL }}
                      />
									</View>
								</View>
							</View>
						</Modal>

            
          </Container>
      </ImageBackground>
      );
   };
}