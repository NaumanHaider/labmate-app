import React from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity, Modal, FlatList } from 'react-native';
import { Header, Content, Footer, Container, Left,  Body, Right,  Title, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import Config from './../Services/Config';

// Components
import CategoryItem from './../components/Product/CategoryItem';
import FooterCopyRight from './../components/Settings/FooterCopyRight';

// Assets
import Icon from 'react-native-vector-icons/FontAwesome';
import StyleGridList from './../styles/GridList';
import customStyles from './../styles/Custom';

export default class Productlines extends React.Component {
  
  	constructor()
	{
		super();

		this.state = {
			isLoading: false,
			refreshing: false,

			listOfCategories: [],

			isMessageModal: false,
      		isMessageModalText: ''
		};
	};

	componentDidMount()
	{
		this.setState({ isLoading: true });

		let api_url = (Config.base_url() + "/api/data/product-line-categories");

		fetch( api_url, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });

			this.setState({
				listOfCategories: res.data
			});
		})
		.catch(error => console.warn(error))
		.done();
	};

	render()
	{
		const { navigate } = this.props.navigation;
		
		return (
		<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

			<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />
			
			<Container style={{ backgroundColor:'transparent'}}>
				<Header style={{ backgroundColor:'#ed3134' }} >
					<Left>
						<TouchableOpacity onPress={()=>this.props.navigation.navigate('DrawerOpen')}>
							<Image 
								source={ require('./../img/icon/menu.png') }
								style={{ height:20, width:20 }}
							/>
						</TouchableOpacity>
					</Left>
					<Body>
						<Title style={{ color:'white', textAlign:'center', alignContent:'center', alignItems:'center', width:206, fontSize:16 }} >Product Lines</Title>
					</Body>
					<Right>
		                <TouchableOpacity>
		                  <Button transparent onPress={() => this.props.navigation.navigate('Home') }>
		                      <Image 
		                      source={ require('./../img/icon/arrow.png') }
		                      style={{ height:12, width:22 }}
		                    />
		                  </Button>
		                </TouchableOpacity>
		              </Right>
				</Header>
				<Content style={{ flex:1 }} >
					
					<View style={ StyleGridList.container }>

						<FlatList
	                    data={this.state.listOfCategories}
	                    renderItem={( {item, index} ) => {
	                      // console.warn( `id=${item.id} index=${index}` );
	                      return (<CategoryItem
	                        key={index} 
	                        row={item}
	                        index={index}
	                        navigation={navigate}
	                      />)
	                    }}
	                    keyExtractor={(item, index) => item.title}
	                    refreshing={true}
	                  />
	                  
                  	</View>

                  	<Modal
						transparent={ true }
						visible={this.state.isMessageModal}
						onRequestClose={ () => console.warn('this is') }
					>
						<View style={ customStyles.modalBody }>
							<View style={ customStyles.modalContainer }>
								<View style={ customStyles.modalBox }>
									<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
											this.setState({
												isMessageModal: false
											})
										}}>
											<Icon name="times-circle" size={18} color="white" />
									</TouchableOpacity>
									<Text style={ customStyles.modalTextMessage }>
										{ this.state.isMessageModalText }
									</Text>
								</View>
							</View>
						</View>
					</Modal>

				</Content>
				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>
			</Container>
		</ImageBackground>
		);
	} 
}