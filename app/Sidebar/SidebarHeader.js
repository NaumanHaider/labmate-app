import React from 'react';
import {Image, TouchableOpacity, AsyncStorage, BackHandler} from 'react-native';
import {Container, Content, Header, Body, Icon, View, Text} from 'native-base';

import Config from './../Services/Config';

/**
 * Firebase Notification :: Top Bar Notification
 */
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from "react-native-fcm";

export default class SidebarHeader extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            full_name: '-',
            email: '-',
            avatar_url: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg',
            user_id: 0,
            fcm_token: ''
        }

        AsyncStorage.getItem('user_id').then((value)  => this.setState({ 'user_id': value }))
    }

    componentDidMount()
	{  
        setInterval( () => {
            AsyncStorage.getItem('name').then((value) => this.setState({ full_name: value }) );
            AsyncStorage.getItem('email').then((value) => this.setState({ email: value }) );
            AsyncStorage.getItem('avatar_url').then((value) => this.setState({ avatar_url: value ? Config.base_url() + value : this.state.avatar_url }) );  

            // Initialize Notification

        }, 3000);

        FCM.requestPermissions()
        // .then(()=>console.warn('granted')).catch(()=>console.warn('notification permission rejected'));
        
        // Generate FCM Token
        FCM.getFCMToken().then( (token) => this.setState({ fcm_token: token }) );

        // Generate FCM+Topic Generate  
        AsyncStorage.getItem('user_id').then((value) => {

            if( value ) {

                let topic = `fcm_token-${value}`;
                
                // console.warn(topic);
                
                this._updateFCMToken( value, topic );

                FCM.subscribeToTopic( topic );
            }
        });

        // If the user is logged-out, we unsubscribe
        // FCM.unsubscribeFromTopic('logged-out');
        
        // INITIAL NOTIFICATION
        FCM.getInitialNotification().then( (notif) => {
            console.warn("INITIAL NOTIFICATION", notif)
        });

        this._notificationConfigured();
    };

    componentWillUnmount() {
        
        // console.warn( 'close application' );
        BackHandler.exitApp();
        
        // this.notificationListner.remove();
        // this.refreshTokenListener.remove();
    }

    /**
     * Notification Iniaitlize
     * - - - - - - - - - - - -
     * 
     */
    _notificationConfigured() {

        FCM.on(FCMEvent.Notification, (notif) => {    

            // console.warn("FCMEvent NOTIFICATION", notif)

            // this._showLocalNotification( notif );
            
            // console.warn( notif.profile );
            if(  notif.collapse_key && (notif.local_notification || notif.opened_from_tray) )
            {
                this._notificationListener( notif );

                notif.finish()
            }
            
        });
    }

    _notificationListener = ( notif ) => {

        let routes = this.props.prop_property;
        // routes.navigation.navigate('About Us');
        
        /**
         * List of Target Screens
         * - - - - - - - - - - - - 
         *
         * - Event Registration
         * - Book An Appointment / Schedule a Meeting
         * - Certificate of Analysis
         * - Get An Enquiry / Technical Support
         * - Quick Quote - Enquiry
         * - Contact Us
         * 
         */
        
        // console.warn( 'target_screen: ', notif.target_screen );

        // Activity :: Event Registration
        if( notif.target_screen && notif.target_screen === 'event-registration')
        {
            routes.navigation.navigate( 'EventRegistration', { event_id: notif.getId } );
        }

        // Activity :: "Book An Appointment" Replace "Schedule a Meeting"
        if( notif.target_screen && notif.target_screen === 'booking-appointment')
        {
            routes.navigation.navigate( 'GetAnAppoinment');
        }

        // Activity :: "Certificate of Analysis"
        if( notif.target_screen && notif.target_screen === 'certificate-of-analysis')
        {
            routes.navigation.navigate( 'CertificateOfAnalysis');
        }

        // Activity :: "Get An Enquiry" Replace "Technical Support"
        if( notif.target_screen && notif.target_screen === 'get-an-enquiry')
        {
            routes.navigation.navigate( 'GetEnquiry');
        }

        // Activity :: "Quick Quote - Enquiry"
        if( notif.target_screen && notif.target_screen === 'quick-quote-enquiry')
        {
            routes.navigation.navigate( 'Your Selection');
        }

        // Activity :: "Contact Us"
        if( notif.target_screen && notif.target_screen === 'contactus')
        {
            routes.navigation.navigate( 'Contact Us');
        }

        if(notif.local_notification)
        {
            return;
        }

        if(notif.opened_from_tray)
        {
            return;
        }
    }

    _showLocalNotification = (notif) => {
        
        // if( Config.is_debug() )
        console.warn("Show Local Notification: ");
        console.warn(notif);
        
        FCM.presentLocalNotification({
            title: notif.title,
            body: notif.body,
            priority: "high",
            click_action: notif.click_action,
            show_in_foreground: true,
            local: false
        });
    }

    /**
     * Update FCM Token
     * - - - - - - - - 
     */
    _updateFCMToken = ( user_id, fcm_token ) => {
        
        console.warn( ` User ID: ${user_id}, fcm_token: ${fcm_token}` );

        fetch( Config.base_url() + "/api/user/update-fcm-token", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ user_id: user_id, fcm_token: fcm_token })
        })
        .then((response) => response.json() )
        .done();
    }

    render()
    {   
        let prop_property = this.props.prop_property;

        // console.warn( prop_property );

        return (
            <Body style={{flex:1, flexDirection: 'row'}}>
				<TouchableOpacity onPress = { () => prop_property.navigation.navigate('ProfileEdit') } >
					<Image 
					style={{ width:80, height:80, borderRadius:40 }}
					source={{ uri: this.state.avatar_url }} 
					/>
				</TouchableOpacity>
				<View style={{ marginLeft:20, marginTop:20 }}>
					<TouchableOpacity onPress = { () => prop_property.navigation.navigate('ProfileEdit') } >
						<Text style={{ fontWeight:'bold', color: '#e8181c' }}>{ this.state.full_name }</Text>
					</TouchableOpacity>
					{/* Text View for email */}
				</View>
			</Body>
        );
    }
}

// <Text style={{ fontSize:14, color: '#808080' }}>{ this.state.email }</Text>