import React, {StyleSheet} from 'react-native';

export default StyleSheet.create({

	err: {
		borderWidth:1,
		borderColor: 'red'
	},

	bodyContainer: {
		flexGrow: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},

	logoContainer: {
		flexGrow: 1,
		justifyContent: 'flex-end',
		paddingVertical: 30,
		alignItems: 'center'
	},

	formContainer: {
		flexGrow: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: 60
	},

	logoText: {
		fontSize: 18,
		color: 'rgba(255, 255, 255, 0.7)'
	},

	loginInputBody: {
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingHorizontal: 8,
		marginVertical: 5,
		flexDirection: 'row',
		
		// Box-Shadow
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.18,
		shadowRadius: 1.00,

		elevation: 1,
	},

	inputField: {
		fontSize: 18,
		width: 250,
		paddingTop:6,
		color: '#ffffff',
		fontWeight: 'bold',
	},

	inputFieldtwo: {
		fontSize: 18,
		width: 270,
		paddingTop:15,
		color: '#ffffff',
		fontWeight: 'bold',
	},

	inputForgot: {
		fontSize: 14,
		fontFamily: 'Roboto-Bold',
		width: 300,
		color: '#ffffff',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingHorizontal: 20,
		paddingVertical: 10,
		marginVertical: 5,
		
		// Box-Shadow
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.18,
		shadowRadius: 1.00,

		elevation: 1,
	},

	touchLoginTButton: {
		width: 300,
		backgroundColor: '#ed3135',
		borderRadius: 25,
		marginVertical: 5,
		paddingVertical: 16,
		
		// Box-Shadow
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},

	textLoginTButton: {
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center'
	},

	modalForgotContainer: {
		alignContent:'center',  
		alignItems:'center', 
		backgroundColor: 'white', 
		width:300, 
		padding:10,
		marginLeft:40,
		marginTop:150,
		borderRadius:15
	},

	forgotFooterButton: {
		width: 138,
		backgroundColor: '#ed3135',
		borderRadius: 25,
		paddingVertical: 12,
		paddingHorizontal: 20,
		marginHorizontal: 10,
		color:'white',
		fontFamily: 'Roboto-Bold',
		fontWeight: 'bold',
		fontSize: 16,
		textAlign:'center',
		
		// Box-Shadow
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.24,
		shadowRadius: 2.22,
		overflow: 'hidden',

		elevation: 3,
	},

	footerContainer: {
		flexGrow: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 26,
		flexDirection: 'row'
	},

	footerDontText: {
		color: '#4c4b4d',
		fontSize: 16,
		fontFamily: 'Roboto-Bold'
	},

	footerSignupButton: {
		color:'#ed3134',
		fontSize: 16,
		fontFamily: 'Roboto-Bold',
		fontWeight: '500'
	}
});