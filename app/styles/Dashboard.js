import React, {StyleSheet} from 'react-native';

export default StyleSheet.create({

	bodyContainer: {
		flex:1,
		justifyContent: 'center',
		alignItems: 'center',
	},

	bodyContainerDashboard: {
		flex:1,
		marginTop: 30,
		alignItems: 'center',
	},

	// Quick Quote / Enquiry
	logoContainer: {
		justifyContent: 'center',
		marginTop: 30,
		// marginRight:-100,
	},

	// Promotions
	logoContainer2: {
		justifyContent: 'center',
		// marginTop: -30,
		marginRight: 10
	},

	// Events
	logoContainer3: {
		// marginLeft: -225,
		marginTop: -30
	},

	// Product-Lines
	logoContainer4: {
		marginTop:-120,
		// marginRight:-220,
	},

	// Quick Support
	logoContainer5: {
		// marginTop:-40,
		marginRight: 5
	},

	normalOpacity5: {
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
	}	
});