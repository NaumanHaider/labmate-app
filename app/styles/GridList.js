import React, {StyleSheet} from 'react-native';
import { Row } from 'native-base';


export default StyleSheet.create({

	container: {
		flex: 1, 
		flexDirection: 'column', 
		padding: 10 
	},

	boxRow: { 
		flex: 2,
		flexDirection: 'row',
		justifyContent:'center',
		marginVertical: 4
	},

	item: { 
		// flexDirection: 'row',
		borderColor: '#e9e9e9', 
		// width: 155,
		borderRadius: 5,
		marginHorizontal:  5,
		height: 155,
	},

	itemText: { 
		flex: 1,
		marginTop: -30,
		color: 'white',
		fontFamily: 'Roboto-Bold',
		fontSize: 13,
		textAlign: 'center',
		marginHorizontal:5,
	}

});