import React, {StyleSheet} from 'react-native';


export default StyleSheet.create({

	bodyContainer: {
		// flex:1,
		alignItems:'center',
		justifyContent: 'center'
	},

	bodySubContainer:{
		flex:1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent: 'center',
		padding:20
	},

	fullInputContainerClass: { flexDirection: 'row', marginVertical:5 },
	fullInputIconClass: { margin:10, height:25, width:25 },

	inputField: {
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 25,
		paddingVertical:12,
		paddingHorizontal:20,
		fontSize: 16,
		color: '#ffffff',
		marginVertical: 10
	},

	fieldTextarea: {
		width: 270,
		textAlignVertical: 'top',
		backgroundColor: 'rgba(152, 152, 148, 0.7)',
		borderRadius: 15,
		fontSize: 16,
		color: '#ffffff',
		paddingVertical:12,
		paddingHorizontal:20,
		marginTop: 10,
		marginBottom:0
	},

	touchBtnSubmit: {
		width: 270,
		backgroundColor: '#ed3134',
		borderRadius: 25,
		marginVertical: 10,
		paddingVertical: 12,
		elevation:3
	},

	btnButtonSubmit: {
		fontSize: 16,
		fontWeight: '500',
		color:'#ffffff',
		textAlign: 'center',
		width: 270

	},

	footerCopyright: {
		paddingTop: 18,
		alignContent:'center', 
		alignItems:'center'
	},

	err: {
		borderWidth:1,
		borderColor: 'red'
	},

	checkBox:
	{
		width:70, 
		position:'absolute', 
		top:35, 
		right:-25
	},

	mainText:{
		fontFamily: 'Roboto-Regular',
		fontWeight:'bold',
		fontSize: 20,
		marginTop:15,
		marginBottom:0,
		color: '#ee3135'
	}
});