export default class Config 
{
    static is_debug() {
        return false;
    }

    static get_password() {
        return 'Super!@#4';
    }

    static base_url() {
        
        /*
        * On Development Server
        */
        // return 'http://192.168.1.104/lab-mate-apps/public_html'; //Home
        // return 'http://192.168.1.103/lab-mate-apps/public_html'; //Home
        // return 'http://192.168.0.105/lab-mate-apps/public_html'; //Kumail
        // return 'http://192.168.0.107/lab-mate-apps/public_html'; //Usama
        // return 'http://192.168.0.107/lab-mate-apps/public_html'; //labmate
        // return 'http://192.168.0.121/lab-mate-apps/public_html'; //labmate


        /*
        * On Stage Server
        */
        // return 'http://clients2.5stardesigners.net/labmate'; //clients2.5stardesigners

        /*
        * On Live Server
        */
        return 'http://www.science-ware.com'; //www.science-ware.com
    }
}