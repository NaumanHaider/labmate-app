export default class ArrayHelper
{
    static unset_arr_by_value(value, arr)
    {
        return arr.map( (x) => {
            return ((x != value) ? x : 0);
        }).filter((x) => x);
   }
}
