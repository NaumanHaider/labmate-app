import React from 'react';
import {ImageBackground, AsyncStorage} from 'react-native';

export default class Logo extends React.Component
{
    constructor()
    {
        super();
        
        setTimeout( () => {
            
            const { navigate } = this.props.navigation;

            AsyncStorage.getItem('user_id')
            .then((item) => {
                if (item) {
                    navigate( 'Dashboard' );
                } else {
                    navigate( 'Login' );
                }
            });

            // navigate( 'Request for Quotation' );
            // navigate( 'Newsroom' );

        }, 3000);
    };

    render()
    {   
        return (
            <ImageBackground source={ require('./../img/splash-screen-bg.gif')} style={{  flex: 1, flexDirection: "row", alignItems: "stretch"}}>
            </ImageBackground>
        );
    }
}