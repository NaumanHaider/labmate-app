import React from 'react';
import {StyleSheet, Text, View, ScrollView, TextInput, Image, TouchableOpacity, ImageBackground,  Modal} from 'react-native';
import { Button } from 'native-base';

import Config from './../Services/Config';
import Validation from './../Services/Validation';

import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker';
import PhotoUpload from 'react-native-photo-upload';
import DeviceInfo from 'react-native-device-info';
import Spinner from 'react-native-loading-spinner-overlay';

import styles from './../styles/Signup';
import customStyles from './../styles/Custom';

import { BackHandler } from 'react-native';
import { KeyboardAvoidingView } from 'react-native';

import {Select, Option} from "react-native-chooser";

export default class Signup extends React.Component
{
	constructor(props)
	{
		super(props);
		this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

		// this.get_validate 	= this.get_validate.bind(this);
		this.btnSubmit 		= this.btnSubmit.bind(this); 
		
		this.state = {
			
			// Validations
			validate_of_first_name: true,
			validate_of_last_name: true,
			validate_of_username: true,
			validate_of_email: true,
			validate_of_mobile: true,
			validate_of_date_of_birth: true,
			validate_of_gender: true,
			validate_of_address: true,
			validate_of_region: true,
			validate_of_city: true,
			validate_of_qualification: true,
			validate_of_profession: true,
			validate_of_organization: true,
			validate_of_designation: true,
			validate_of_department: true,
			validate_of_password: true,
			validate_of_confirm_password: true,

			// Messsages
			validate_of_first_name_msg: '',
			validate_of_last_name_msg: '',
			validate_of_username_msg: '',
			validate_of_email_msg: '',
			validate_of_mobile_msg: '',
			validate_of_date_of_birth_msg: '',
			validate_of_gender_msg: '',
			validate_of_address_msg: '',
			validate_of_region_msg: '',
			validate_of_city_msg: '',
			validate_of_qualification_msg: '',
			validate_of_profession_msg: '',
			validate_of_organization_msg: '',
			validate_of_designation_msg: '',
			validate_of_department_msg: '',
			validate_of_password_msg: '',
			validate_of_confirm_password_msg: '',
			
			// Set of data
			first_name: '',
			last_name: '',
			username: '',
			email: '',
			mobile: '',
			date_of_birth: '',
			address: '',
			gender: 'Male',

			region_title: 'Select City',
			region_id: '',

			city: '',
			qualification: '',
			profession: '',
			organization: '',
			designation: '',
			department: '',
			password: '',
			confirm_password: '',
			avatar: '',

			auth: null,
			is_auth: false,

			isLoading: false,

			isMessageModal: false,
			isMessageModalText: '',

			getRegions: []
		};
	};

	omponentWillMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
	}
	
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
	}
	
	handleBackButtonClick() {
		this.props.navigation.goBack(null);
		return true;
	}

	componentDidMount()
	{
		this._getRegions();
    };

	_form_validation()
	{
		const _scrollView = this.scrollView;

		let first_name 		= this.state.first_name;
		let last_name 		= this.state.last_name;
		let username 		= this.state.username;
		let email 			= this.state.email;
		let mobile 			= this.state.mobile;
		let date_of_birth 	= this.state.date_of_birth;
		let address 		= this.state.address;
		let region_id 		= this.state.region_id;
		let city 			= this.state.city;
		let qualification 	= this.state.qualification;
		let profession 		= this.state.profession;
		let organization 	= this.state.organization;
		let designation 	= this.state.designation;
		let department 		= this.state.department;
		let password 		= this.state.password;
		let confirm_password = this.state.confirm_password;
		let result 			= true;
		let scrollTo 		= 0;

		// First Name
		if( first_name != "" && Validation.isOnlyString(first_name) )
		{
			this.setState({ validate_of_first_name: true });
		}
		else
		{
			this.setState({ validate_of_first_name: false });
			result = false;
		}

		this.setState({ validate_of_first_name_msg: (
			(first_name == "" ) ? 
			'This field is required' : 
			(!Validation.isOnlyString(first_name) ? 'Only letters are allowed' : '')
		) });

		// Last Name
		if( last_name != "" && Validation.isOnlyString(last_name) )
		{
			this.setState({ validate_of_last_name: true });
		}
		else
		{
			this.setState({ validate_of_last_name: false });
			result = false;
		}

		this.setState({ validate_of_last_name_msg: (
			(last_name == "" ) ? 
			'This field is required' : 
			(!Validation.isOnlyString(last_name) ? 'Only letters allowed' : '')
		) });
		
		// Username
		if( username != "" && Validation.isOnlyStringNumeric(username) )
		{
			this.setState({ validate_of_username: true });
		}
		else
		{
			this.setState({ validate_of_username: false });
			result = false;
		}

		this.setState({ validate_of_username_msg: (
			(username == "" ) ? 
			'This field is required' : 
			(!Validation.isOnlyStringNumeric(username) ? 'Only alpha - numerice allowed' : '')
		) });

		// Email Address
		if( email != "" && Validation.isEmail(email) )
		{
			this.setState({ validate_of_email: true });
		}
		else
		{
			this.setState({ validate_of_email: false });
			result = false;
		}

		this.setState({ validate_of_email_msg: (
			(email == "" ) ? 
			'This field is required' : 
			(!Validation.isEmail(email) ? 'Email format should be joe@example.com' : '')
		) });

		// Phone
		if( mobile != "" && Validation.isNumber(mobile) )
		{
			this.setState({ validate_of_mobile: true });
		}
		else
		{
			this.setState({ validate_of_mobile: false });
			result = false;
		}

		this.setState({ validate_of_mobile_msg: (
			(mobile == "" ) ? 
			'This field is required' : 
			(!Validation.isNumber(mobile) ? 'Mobile format should be numeric' : '')
		) });

		

		this.setState({ validate_of_date_of_birth: true, validate_of_date_of_birth_msg: '' });

		// Date of Birth
		if( date_of_birth != "" )
		{
			let split_date_of_birth 	= date_of_birth.split('-');
			let user_date_of_birth 		= new Date( split_date_of_birth[2] + '-' + split_date_of_birth[1] + '-' + split_date_of_birth[0] ).getFullYear();
			let user_current_of_birth 	= new Date().getFullYear();
			let diff_of_age 			= ( ((user_current_of_birth - user_date_of_birth) < 18) ? true : false );
			
			if( diff_of_age )
			{
				this.setState({ validate_of_date_of_birth: false, validate_of_date_of_birth_msg: 'Your age should morethan 18 years ' });
				result = false;
			}
		}
		else
		{
			this.setState({ validate_of_date_of_birth: false, validate_of_date_of_birth_msg: 'This field is required' });

			result = false;
		}

		// Address
		if( !Validation.isNotAllowSpecialCharacters(address) )
		{
			this.setState({ validate_of_address: true });
		}
		else
		{
			this.setState({ validate_of_address: false });
			result = false;
		}

		this.setState({ validate_of_address_msg: Validation.isNotAllowSpecialCharacters(address) ? 'This format is invalid' : '' });

		// Region
		if( region_id != "" )
		{
			this.setState({ validate_of_region: true });
		}
		else
		{
			this.setState({ validate_of_region: false });
			result = false;
		}

		this.setState({ validate_of_region_msg: region_id == ""  ?  'Please Select City' : '' });

		// Qualification
		if( !Validation.isNotAllowSpecialCharacters(qualification) )
		{
			this.setState({ validate_of_qualification: true });
		}
		else
		{
			this.setState({ validate_of_qualification: false });
			result = false;
		}

		this.setState({ validate_of_qualification_msg: Validation.isNotAllowSpecialCharacters(qualification) ? 'This format is invalid' : '' });

		// Profession
		if( !Validation.isNotAllowSpecialCharacters(profession) )
		{
			this.setState({ validate_of_profession: true });
		}
		else
		{
			this.setState({ validate_of_profession: false });
			result = false;
		}

		this.setState({ validate_of_profession_msg: Validation.isNotAllowSpecialCharacters(profession) ? 'This format is invalid' : '' });

		// Organization
		if( !Validation.isNotAllowSpecialCharacters(organization) )
		{
			this.setState({ validate_of_organization: true });
		}
		else
		{
			this.setState({ validate_of_organization: false });
			result = false;
		}

		this.setState({ validate_of_organization_msg: Validation.isNotAllowSpecialCharacters(organization) ? 'This format is invalid' : '' });
		
		// Designation
		if( !Validation.isNotAllowSpecialCharacters(designation) )
		{
			this.setState({ validate_of_designation: true });
		}
		else
		{
			this.setState({ validate_of_designation: false });
			result = false;
		}

		this.setState({ validate_of_designation_msg: Validation.isNotAllowSpecialCharacters(designation) ? 'This format is invalid' : '' });

		// Department
		if( !Validation.isNotAllowSpecialCharacters(department) )
		{
			this.setState({ validate_of_department: true });
		}
		else
		{
			this.setState({ validate_of_department: false });
			result = false;
		}

		this.setState({ validate_of_department_msg: Validation.isNotAllowSpecialCharacters(department) ? 'This format is invalid' : '' });

		// Password
		if( password != "" )
		{
			if( password.length < 8 ){
				this.setState({ validate_of_password: false });
				this.setState({ validate_of_password_msg: 'Password must contain alpha-numeric and must be 8-16 characters long' });
				result = false;
			}

			else if( !Validation.isPassword(password) ){
				this.setState({ validate_of_password: false }); 
				this.setState({ validate_of_password_msg: 'Password must contain alpha-numeric and must be 8-16 characters long' });
				result = false;
			}

			else {
				this.setState({ validate_of_password: true }); 
				this.setState({ validate_of_password_msg: '' });
			}
		}
		else
		{
			this.setState({ validate_of_password: false });
			this.setState({ validate_of_password_msg: 'This field is required' });
			result = false;
		}

		// Confirm Password
		if( confirm_password != "" )
		{
			this.setState({ validate_of_confirm_password: true });

			// Match The Password
			if( password != "" && confirm_password != "" && ( password == confirm_password) )
			{
				this.setState({ validate_of_confirm_password: true, validate_of_confirm_password_msg: '' });
			}
			else
			{
				this.setState({  validate_of_confirm_password: false, validate_of_confirm_password_msg: 'Password do not match' });
				result = false;
			}
		}
		else
		{
			this.setState({ validate_of_confirm_password: false, validate_of_confirm_password_msg: 'This field is required' });
			result = false;
		}

		// alert(scrollTo);
		_scrollView.scrollTo({x: 0, y: 0, animated: true})

		return result;
	}

	_handleOfResetForm = () => {

		this.setState({
			
			// Set of data
			first_name: '',
			last_name: '',
			username: '',
			email: '',
			mobile: '',
			date_of_birth: '',
			address: '',
			gender: 'Male',
			region_title: 'Select City',
			region_id: '',

			city: '',
			qualification: '',
			profession: '',
			organization: '',
			designation: '',
			department: '',
			password: '',
			confirm_password: '',
			avatar: ''
		});
	}

	btnSubmit = ( navigate ) => {

		let is_validate = this._form_validation();

		if( is_validate )
		{
			let device_id 	= DeviceInfo.getUniqueID();
			let device_name = DeviceInfo.getSystemName() + ' - ' + DeviceInfo.getManufacturer() + ' - ' + DeviceInfo.getModel() + ' - ' + DeviceInfo.getSystemVersion();

			let postdata = {

				first_name: this.state.first_name,
				last_name: this.state.last_name,
				username: this.state.username,
				email: this.state.email,
				mobile: this.state.mobile,
				password: this.state.password,
				address: this.state.address,
				date_of_birth: this.state.date_of_birth,
				gender: this.state.gender,
				avatar: this.state.avatar,
				region_id: this.state.region_id,
				
				city: this.state.city,
				qualification: this.state.qualification,
				profession: this.state.profession,
				organization: this.state.organization,
				designation: this.state.designation,
				department: this.state.department,

				device_id: device_id,
				device_name: device_name
			};

			this._ajaxFormServerRequest( postdata, navigate );
		}
	};

	_ajaxFormServerRequest = (postdata, navigate) => {

		this.setState({ isLoading: true });

		fetch( Config.base_url() + "/api/user/signup", {
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(postdata)
		})
		.then((response) => response.json() )
		.then((res) => {

			this.setState({ isLoading: false });

			if( res.status )
			{
				this._handleOfResetForm();

				navigate('Login');
			}

			this.setState({
				password: '',
				confirm_password: '',

				isMessageModal: true,
				isMessageModalText: res.message
			});

		})
		.catch( (error) => console.warn(error) )
		.done();
	}

	_getRegions = () => {

		fetch( Config.base_url() + "/api/data/regions", {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then((response) => response.json() )
		.then((res) => {

			if( res.status )
			{
				let regions 		= res.data;
				let first_element  	= ( regions.length ? regions[0] : '' );

				// region_title: ( first_element.title || 'City' ),
				// region_id: ( first_element.id || 0 ),
					
				this.setState({ 
					getRegions: regions 
				});
			}

		})
		.catch( (error) => console.warn(error) )
		.done();
	}
    render()
    {
		const { navigate } 	= this.props.navigation;
	
        return(
			<KeyboardAvoidingView  behavior="padding" enabled style={{
				flex: 1
			  }}>
			<ImageBackground source={require('./../img/common-background.png')} style={styles.bodyContainer}>
				
				<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

				<ScrollView
					ref={scrollView => this.scrollView = scrollView}
					keyboardShouldPersistTaps="always"
					contentContainerStyle={{
						flexGrow: 1
					}}
				>
				
					<View style={styles.topHeader}>
						<Text style={styles.mainText}>Create Account</Text>
						 <PhotoUpload
							// onPhotoSelect={avatar => {
							// 	alert("Selected");
							// 		if (avatar) {
							// 			this.setState({ avatar:  avatar});
							// 		}
							// 	}}
								onResponse={response => {							
									if (response.didCancel) {
										console.log('User cancelled image picker');
									} else if (response.error) {
										console.log('ImagePicker Error: ', response.error);
									} else if (response.customButton) {
										console.log('User tapped custom button: ', response.customButton);
									} else {
										this.setState({
										avatar: response.uri,
										});
									}
									// alert("did it " + response.uri);
								}}
								>
								<Image
									style={{
										paddingVertical: 30,
										width: 120,
										height: 120,
										borderRadius: 60,
										marginTop: 30
									}}
									resizeMode='cover'
									source={{
										uri: this.state.avatar == '' ? 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg' : this.state.avatar
									}}
								/>
						</PhotoUpload>
					</View>

					<View style={{ flexGrow:1, flexDirection: 'row', justifyContent: 'center'}}>
					
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/name-icon.png')} style={{ width:15, height:15 }} />
							</View>
							<TextInput 
								style={ [styles.inputField, !this.state.validate_of_first_name ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								placeholder= 'First Name'
								placeholderTextColor= '#000'
								value={ this.state.first_name }
								onChangeText={(text) => this.setState({'first_name': text})}
								minLength={5}
								maxLength={20}
							/>
						</View>

						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/name-icon.png')} style={{ width:15, height:15 }} />
							</View>
							<TextInput 
								style={ [styles.inputField, !this.state.validate_of_last_name ? styles.err:null] } 
								minLength={3}
								maxLength={15}
								autoCapitalize={'words'}
								underlineColorAndroid='transparent'
								placeholder= 'Last Name'
								placeholderTextColor= '#000'
								value={ this.state.last_name }
								onChangeText={(text) => this.setState({'last_name': text})}
							/>
						</View>

					</View>

					<View style={{ flexDirection: 'row', marginHorizontal:20}}>
					
						<View style={{width:100}}>
							<Text style={ !this.state.validate_of_first_name ? customStyles.usama_two:null }>{ this.state.validate_of_first_name_msg }</Text>
						</View>

						<View style={{width:100, paddingHorizontal: 20}}>
							<Text style={ !this.state.validate_of_last_name ? customStyles.usama_two:null }>{ this.state.validate_of_last_name_msg }</Text>
						</View>

					</View>

					<View style={{ flex:-1, flexDirection: 'column'}}>

						{/* Username */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/username-icon.png')} style={{ width:15, height:15 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_username ? styles.err:null] }
								underlineColorAndroid='transparent'
								placeholder= 'Username'
								autoCapitalize="none"
								placeholderTextColor= '#000'
								value={ this.state.username }
								onChangeText={(text) => this.setState({'username': (text ? text.toLowerCase(): '') })}
								minLength={5}
								maxLength={20}
							/>
						</View>

						<View style={{ marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_username ? customStyles.usama:null }>{ this.state.validate_of_username_msg }</Text>
						</View>

						{/* Email Address */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/creat-account-email-icon.png')} style={{ width:15, height:10, marginTop:5 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_email ? styles.err:null] } 
								underlineColorAndroid='transparent'
								placeholder= 'Email'
								autoCapitalize="none"
								keyboardType="email-address"
								placeholderTextColor= '#000'
								value={ this.state.email }
								onChangeText={(text) => this.setState({'email': text})}
							/>
							
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_email ? customStyles.usama:null }>{ this.state.validate_of_email_msg }</Text>
						</View>

						{/* Mobile Number */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/mobile-icon.png')} style={{ width:10, height:20, marginLeft: 2 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_mobile ? styles.err:null] } 
								underlineColorAndroid='transparent'
								placeholder= 'Mobile'
								keyboardType = 'numeric'
								placeholderTextColor= '#000'
								value={ this.state.mobile }
								onChangeText={(text) => this.setState({'mobile': text})}
								maxLength={15}
							/>
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_mobile ? customStyles.usama:null }>{ this.state.validate_of_mobile_msg }</Text>
						</View>
						
						{/* Date of Birth */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Icon name="calendar" size={15} color="#495057" style={{ marginTop: 5 }} />
							</View>
							<DatePicker
								style={ [styles.inputDateField, !this.state.validate_of_date_of_birth ? styles.err:null] } 
								mode="date"
								showIcon={false}
								date={this.state.date_of_birth}
								value={ this.state.date_of_birth }
								placeholder= 'Date of Birth'
								format="DD-MM-YYYY"
								confirmBtnText="Confirm"
								cancelBtnText="Cancel"
								customStyles={{
									dateIcon: {
										width: 0, 
										height: 0
									},
									dateInput: {
										borderWidth: 0,
										alignItems: 'flex-start'
									},
									placeholderText: {
										fontFamily: 'Roboto-Bold',
										fontWeight: '500',
										fontSize: 13,
										color: '#000'
									},
									dateText: {
										fontFamily: 'Roboto-Bold',
										fontWeight: '500',
										fontSize: 13,
										color: '#000'
									}
								}}
								onDateChange={(date) => {this.setState({date_of_birth: date})}}
							/>	
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_date_of_birth ? customStyles.usama:null }>{ this.state.validate_of_date_of_birth_msg }</Text>
						</View>
						
						{/* Gender */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/gender-icon.png')} style={{ width:16, height:16, marginTop: 0 }} />
							</View>
							<Select
								onSelect = { (text) => this.setState({ gender: text })}
								defaultText  = { this.state.gender }
								style = {{ borderColor: 'transparent', borderBottomColor: '#000', borderBottomWidth: 1, width: 260,  }} 
								textStyle = {{ fontFamily: 'Roboto-Bold', fontWeight: '500', fontSize: 13, color: '#000', }}
								optionListStyle = {{ borderColor: 'transparent', backgroundColor: '#fff', height: 250 }}
								transparent={true}
							>
								<Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} value= "Male">Male</Option>
								<Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} value= "Female">Female</Option>
							</Select>
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_gender ? customStyles.usama:null }>{ this.state.validate_of_gender_msg }</Text>
						</View>
						
						{/* Address */}
						<View style={ [styles.fullInputContainerClass] }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/address-icon.png')} style={{ width:12, height:15, marginLeft: 3 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_address ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								placeholder= 'Address'
								placeholderTextColor= '#000'
								value={ this.state.address }
								onChangeText={(text) => this.setState({'address': text})}
								minLength={3}
								maxLength={100}
							/>
						</View>
						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_address ? customStyles.usama:null }>{ this.state.validate_of_address_msg }</Text>
						</View>

						{/* Region */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/city-icon.png')} style={{ width:16, height:20, marginTop: -5, marginHorizontal:2 }} />
							</View>
							<Select
								onSelect = { (value, text) => this.setState({  region_id: value, region_title: text }) }
								defaultText  = { this.state.region_title }
								style = {[{ borderColor: 'transparent', borderBottomColor: '#000', borderBottomWidth: 1, width: 260}, , !this.state.validate_of_region ? styles.err:null]} 
								textStyle = {{ fontFamily: 'Roboto-Bold', fontWeight: '500', fontSize: 13, color: '#000' }}
								optionListStyle = {{ borderColor: 'transparent', backgroundColor: '#fff', height: 200 }}
								transparent={true}
							>
								<Option value="" style={{ borderBottomColor: '#000', borderBottomWidth: 1 }}>City</Option>
								{ this.state.getRegions.map( (value, index) => <Option style={{ borderBottomColor: '#000', borderBottomWidth: 1 }} key={index} value={value.id}>{value.title}</Option> ) }
							</Select>
						</View>
						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_region ? customStyles.usama:null }>{ this.state.validate_of_region_msg }</Text>
						</View>
						
						{/* Qualification */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/qualification-icon.png')} style={{ width:14, height:16, marginTop: 2, marginLeft: 3 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_qualification ? styles.err:null] } 
								underlineColorAndroid='transparent'
								placeholder= 'Qualification'
								autoCapitalize="words"
								placeholderTextColor= '#000'
								value={ this.state.qualification }
								onChangeText={(text) => this.setState({'qualification': text})}
								minLength={3}
								maxLength={50}
							/>
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_qualification ? customStyles.usama:null }>{ this.state.validate_of_qualification_msg }</Text>
						</View>

						{/* Profession */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/profession-icon.png')} style={{ width:12, height:18, marginTop: 2, marginLeft: 4 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_profession ? styles.err:null] } 
								underlineColorAndroid='transparent'
								placeholder= 'Profession'
								autoCapitalize="words"
								placeholderTextColor= '#000'
								value={ this.state.profession }
								onChangeText={(text) => this.setState({'profession': text})}
								minLength={3}
								maxLength={50}
							/>
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_profession ? customStyles.usama:null }>{ this.state.validate_of_profession_msg }</Text>
						</View>

						{/* Organization */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/organizetion-icon.png')} style={{ width:17, height:15, marginTop: 2, marginLeft: 2 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_organization ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								placeholder= 'Organization'
								placeholderTextColor= '#000'
								value={ this.state.organization }
								onChangeText={(text) => this.setState({'organization': text})}
								minLength={3}
								maxLength={50}
							/>
							
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_organization ? customStyles.usama:null }>{ this.state.validate_of_organization_msg }</Text>
						</View>

						{/* Designation */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/designation-icon.png')} style={{ width:15, height:18, marginTop: 2, marginLeft: 4 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_designation ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								placeholder= 'Designation'
								placeholderTextColor= '#000'
								value={ this.state.designation }
								onChangeText={(text) => this.setState({'designation': text})}
								minLength={3}
								maxLength={50}
							/>
							
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_designation ? customStyles.usama:null }>{ this.state.validate_of_designation_msg }</Text>
						</View>

						{/* Department */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/department-icon.png')} style={{ width:15, height:17, marginTop: 2, marginLeft: 4 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_department ? styles.err:null] } 
								underlineColorAndroid='transparent'
								autoCapitalize="words"
								placeholder= 'Department'
								placeholderTextColor= '#000'
								value={ this.state.department }
								onChangeText={(text) => this.setState({'department': text})}
								minLength={3}
								maxLength={50}
							/>
							
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_department ? customStyles.usama:null }>{ this.state.validate_of_department_msg }</Text>
						</View>

						{/* Password */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/password-icon.png')} style={{ width:14, height:14, marginTop: 2, marginLeft: 4 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_password ? styles.err:null]} 
								underlineColorAndroid='transparent'
								placeholder= 'Password'
								secureTextEntry={true}
								placeholderTextColor= '#000'
								value={ this.state.password }
								onChangeText={(text) => this.setState({'password': text})}
								minLength={3}
								maxLength={15}
							/>
						</View>
						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_password ? customStyles.usama:null }>{ this.state.validate_of_password_msg }</Text>
						</View>

						{/* Confirm Password */}
						<View style={ styles.fullInputContainerClass }>
							<View style={ styles.fullInputIconClass }>
								<Image source={require('./../img/icon/confirm-password-icon.png')} style={{ width:12, height:14, marginTop: 2, marginLeft: 6 }} />
							</View>
							<TextInput 
								style={ [styles.inputField_2, !this.state.validate_of_confirm_password ? styles.err:null] } 
								underlineColorAndroid='transparent'
								placeholder= 'Confirm Password'
								secureTextEntry={true}
								placeholderTextColor= '#000'
								value={ this.state.confirm_password }
								onChangeText={(text) => this.setState({'confirm_password': text})}
								minLength={8}
								maxLength={16}
							/>
						</View>

						<View style={{marginHorizontal:40}}>
							<Text style={ !this.state.validate_of_confirm_password ? customStyles.usama:null }>{ this.state.validate_of_confirm_password_msg }</Text>
						</View>
					</View>

					<View style={{ flex:1, flexDirection: 'row', alignItems: 'center', marginHorizontal: 25, marginVertical: 30 }}>
						<Button  style={styles.touchLoginTButton} onPress = {() => this.btnSubmit( navigate ) }  >
							<Text 
								style={styles.textLoginTButton}
								
							>Register Now</Text>
						</Button>
					</View>
				</ScrollView>

				<Modal
					transparent={ true }
					visible={this.state.isMessageModal}
					onRequestClose={ () => console.warn('this is') }
					>
						<View style={ customStyles.modalBody }>
							<View style={ customStyles.modalContainer }>
								<View style={ customStyles.modalBox }>
									<TouchableOpacity style={ customStyles.closeModalContainer } onPress={ () => {
											this.setState({
												isMessageModal: false
											})
										}}>
											<Icon name="times-circle" size={18} color="white" />
									</TouchableOpacity>
									<Text style={ customStyles.modalTextMessage }>
										{ this.state.isMessageModalText }
									</Text>
								</View>
							</View>
						</View>
					</Modal>

			</ImageBackground>
			</KeyboardAvoidingView>
		);
    };
}