import React from 'react';
import {Text, View, TextInput, Image, ImageBackground, TouchableOpacity, Modal, AsyncStorage, KeyboardAvoidingView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';

// Custom Helpers
import Config from './../Services/Config';
import Validation from './../Services/Validation';

// Assets
import styles from './../styles/Login';
import Custom from './../styles/Custom';

export default class Login extends React.Component
{
	constructor()
	{
		super();

		this.loginAuth 		= this.loginAuth.bind(this); 
		this.isForgotButton = this.isForgotButton.bind(this); 
		
		this.state = {
			
			// Validations
			validate_of_email: true,
			validate_of_password: true,
			is_validate: false,

			// Set of data
			email_address: (Config.is_debug() ? 'alikhan@labmate.com' : ''),
			password: Config.is_debug() ? 'welcome123' : '',

			auth: null,
			is_auth: false,

			isForgotModal: false,
			forgot_email: '',
			validate_of_forgot_email: true,

			// Dymanic Modal
			isMessageModal: false,
			isMessageModalText: '',

			isLoading: false
		};
	};

	_hanldeLoginFormValidation()
	{
		let email 		= this.state.email_address;
		let password 	= this.state.password;

		this.setState({ validate_of_email: ((email != "" && Validation.isEmail(email) ) ? true : false) });
		this.setState({ validate_of_password: ((password != "") ? true : false) });
	}

	loginAuth = (email, pass, navigate) => {

		this._hanldeLoginFormValidation();

		let isValidateLogin = (
			(this.state.email_address || this.state.password) && 
			(this.state.validate_of_email && this.state.validate_of_password )
		);

		if( isValidateLogin )
		{
			let postdata = {
				email: this.state.email_address,
				password: this.state.password
			}

			this.setState({ isLoading: true });

			fetch( Config.base_url() + "/api/user/login", {
				method: "POST",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(postdata)
			})
			.then((response) => response.json() )
			.then((res) => {

				this.setState({ isLoading: false, is_auth: false });

				if( res.status )
				{
					let user = res.data.user;

					this.setState({
						visible: false,
						is_auth: true,
						auth: user
					});

					AsyncStorage.setItem("is_login", "true".toString() );
					AsyncStorage.setItem("user_id", user.id.toString() );
					AsyncStorage.setItem("name", user.first_name +' '+ user.last_name );
					AsyncStorage.setItem("email", user.email );
					AsyncStorage.setItem("avatar_url", user.avatar_url );

					setTimeout( () => {
						
						this.setState({
							isMessageModal: false
						});

						navigate("Dashboard");
						
					}, 1500);
				} 
				// else {
				// 	alert("Incorrect Username OR Password");
						
				// 	this.setState({
				// 		isMessageModal: false
				// 	});
				// }


				// alert(JSON.serialize(res));

				this.setState({
					isMessageModal: true,
					isMessageModalText: res.message
				});
			})
			.catch( (error) => {console.warn(error); alert(error)} )
			.done();
		}
	};
	
	isForgotButton = () => {

		let email 		= this.state.forgot_email;

		this.setState({ validate_of_forgot_email: ((email != "" && Validation.isEmail(email) ) ? true : false) });

		let is_validate = ( email && this.state.validate_of_forgot_email);
		
		if( is_validate )
		{
			this.setState({ isLoading: true });

			fetch( Config.base_url() + "/api/user/forgot", {
				method: "POST",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({ email: email })
			})
			.then((response) => response.json() )
			.then((res) => {

				this.setState({ isLoading: false });

				let status = res.status;

				this.setState({
					validate_of_forgot_email: ( status ? true : false),
					isForgotModal: ( !status ? true : false),
					forgot_email: ( !status ? email : ''),
					isMessageModal: true,
					isMessageModalText: res.message 
				});
			})
			.catch( (error) => console.warn(error) )
			.done();
		}
	};
	
	render() {

		const { navigate } = this.props.navigation;
		
		return(

			<KeyboardAvoidingView  behavior="padding" enabled style={{
				flex: 1
			  }}>
			<ImageBackground source={require('./../img/common-background.png')} style={{ flex: 1 }} >

				<View style={styles.bodyContainer}>
				
					<View style={styles.logoContainer}>
						<Image source={require('./../img/labe-mate-logo.png')} style={{ width:150, height:120, marginTop: 20 }} />
					</View>

					<View style={styles.formContainer}>

						{/* Email Address */}
						<View style={ [styles.loginInputBody, !this.state.validate_of_email ? styles.err:null] } >
							<View style={{ marginVertical: 15, marginHorizontal: 12 }}>
								<Image source={require('./../img/icon/email-icon.png')} style={{ width:15, height:10, marginTop:3 }} />
							</View>
							<TextInput 
								style={ styles.inputField }
								autoCapitalize="none"
								keyboardType="email-address"
								underlineColorAndroid='transparent'
								placeholder= 'Email'
								placeholderTextColor= '#ffffff'
								// value={ this.state.email_address }
								onChangeText = { (text) => this.setState({ email_address: text }) }
							/>
						</View>

						{/* Password */}
						<View style={ [styles.loginInputBody, !this.state.validate_of_password ? styles.err:null] } >
							<View style={{ marginVertical: 15, marginHorizontal: 12 }}>
								<Image source={require('./../img/icon/lock-icon.png')} style={{ width:14, height:17, marginTop: 1 }} />
							</View>
							<TextInput 
								style={ styles.inputField } 
								underlineColorAndroid='transparent'
								placeholder= 'Password'
								autoCapitalize="none"
								secureTextEntry={true}
								placeholderTextColor= '#ffffff'
								// value={ this.state.password }
								onChangeText = { (text) => this.setState({ password: text }) }
							/>
						</View>
						
						<TouchableOpacity 
							style={styles.touchLoginTButton}
							onPress = {() => this.loginAuth(this.state.email_address, this.state.password, navigate ) } 
						>
							<Text style={styles.textLoginTButton}>Login</Text>
						</TouchableOpacity>

						<View style={{ marginVertical: 5 }}>
							<TouchableOpacity onPress={ () => navigate('Forgot') }>
								<Text style={{ fontSize: 18, textAlign: 'center', fontFamily: 'Roboto-Bold', color: '#4c4b4d' }} onPress={ () => {
									this.setState({ isForgotModal: true, forgot_email: '', validate_of_forgot_email: true })
								}} >Forgot Password</Text>
							</TouchableOpacity>
						</View>

					</View>

					<View style={[styles.footerContainer, {marginBottom: 50}]}>
						<Text style={styles.footerDontText}>Don't Have An Account yet? </Text>
						<TouchableOpacity onPress={ () => navigate('Signup') }>
							<Text style={styles.footerSignupButton}>Sign Up</Text>
						</TouchableOpacity>
					</View>

					{/* Forgot Email Modal */}
					<Modal
						transparent={ true }
						//onOrientationChange='overFullScreen'
						visible={this.state.isForgotModal}
						onRequestClose={ () => console.warn('this is') }
					>
						<View style={{ 
							flex: 1, 
							backgroundColor: 'rgba(52, 52, 52, 0.8)'
						}}>
							<View style={{ flexGrow:1, flexDirection: 'column', alignItems: 'center',  marginVertical: 50 }}>

								<View style={{  backgroundColor: 'white', width:350, borderRadius:15 }}>
									<View style={{ flexDirection:'column', alignContent: 'center', alignItems: 'center' }}>
										<Text style={{fontFamily: 'Roboto-Bold', fontSize: 20, fontWeight:'bold', textAlign:'center', color: '#ed3135', marginVertical: 20}} >Forgot Password?</Text>
										<Text style={{fontFamily: 'Roboto-Bold', color: '#4c4b4d', fontSize: 16, textAlign: 'center', paddingHorizontal: 20}}>Enter your email address to request a password reset.</Text>
										<TextInput 
											style={ [styles.inputForgot, {paddingVertical: 10, marginTop: 30}, !this.state.validate_of_forgot_email ? styles.err:null] } 
											underlineColorAndroid='transparent'
											keyboardType="email-address"
											placeholder= 'Enter your email address'
											placeholderTextColor= '#ffffff'
											onChangeText = { (text) => this.setState({ forgot_email: text }) }
											// value={ }//this.state.forgot_email }
										/>
									</View>
									<View style={{ flexDirection: 'row', alignSelf: 'center', marginVertical:15 }}>

										<TouchableOpacity onPress={ () => {
											this.setState({
												isForgotModal: false
											})
										}}>
											<Text style={ styles.forgotFooterButton }>Cancel</Text>
										</TouchableOpacity>
										<TouchableOpacity onPress={ () => this.isForgotButton() } >
											<Text style={ [styles.forgotFooterButton] } >Request</Text>
										</TouchableOpacity>
									</View>							
								</View>

							</View>
						</View>
						<Modal
							transparent={ true }
							visible={this.state.isMessageModal}
							onRequestClose={ () => console.warn('this is') }
						>
							<View style={ Custom.modalBody }>
								<View style={ Custom.modalContainer }>
									<View style={ Custom.modalBox }>
										{ (this.state.is_auth == false) ? 
										<TouchableOpacity style={ Custom.closeModalContainer } onPress={ () => {
												this.setState({
													isMessageModal: false
												})
											}}>
												<Icon name="times-circle" size={18} color="white" />
										</TouchableOpacity>
										: null }
										<Text style={ Custom.modalTextMessage }>
											{ this.state.isMessageModalText }
										</Text>
									</View>
								</View>
							</View>
						</Modal>
					</Modal>
					
					{/* Modal Message */}
					<Modal
						transparent={ true }
						visible={this.state.isMessageModal}
						onRequestClose={ () => console.warn('this is') }
					>
						<View style={ Custom.modalBody }>
							<View style={ Custom.modalContainer }>
								<View style={ Custom.modalBox }>
									{ (this.state.is_auth == false) ? 
									<TouchableOpacity style={ Custom.closeModalContainer } onPress={ () => {
											this.setState({
												isMessageModal: false
											})
										}}>
											<Icon name="times-circle" size={18} color="white" />
									</TouchableOpacity>
									: null }
									<Text style={ Custom.modalTextMessage }>
										{ this.state.isMessageModalText }
									</Text>
								</View>
							</View>
						</View>
					</Modal>


				<Spinner style={{ zIndex:222 }} visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

				</View>
			</ImageBackground>
			</KeyboardAvoidingView>
		);	
	}
}