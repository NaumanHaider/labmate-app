import React from 'react';
import {Text, View, Image, ImageBackground, Animated, TouchableOpacity, AsyncStorage, Linking} from 'react-native';
import {Footer} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

import Config from './../Services/Config';

// Custom Components
import FooterCopyRight from './../components/Settings/FooterCopyRight';

// Styles
import styles from './../styles/Dashboard';

export default class Dashboard extends React.Component
{
	static navigationOptions = {
		drawerIcon: (
		<Image 
			source={ require('./../img/icon/home-icon.png') }
			style={{ height:20, width:20 }}
		/>
		)
	}

	constructor()
	{
		super();

		this.state = {
			dataSource: null,
			isLoading: false,
			
			animatedQuickQuotePosition: new Animated.Value(-300),
			animatedProductlinePosition: new Animated.Value(-300),
			animatedPromotionPosition: new Animated.Value(0),
			animatedEventsPosition: new Animated.Value(-300),
			animatedQuickSupportPosition: new Animated.Value(0)
		};
	};

	componentWillMount = () => {
		
		Animated.timing(this.state.animatedQuickQuotePosition, {
			toValue: -180,
			duration: 1000
		 }).start()

		 Animated.timing(this.state.animatedProductlinePosition, {
			toValue: -200,
			duration: 1000
		 }).start()

		 Animated.timing(this.state.animatedPromotionPosition, {
			toValue: -30,
			duration: 1000
		 }).start()

		 Animated.timing(this.state.animatedEventsPosition, {
			toValue: -210,
			duration: 1000
		 }).start()

		 Animated.timing(this.state.animatedQuickSupportPosition, {
			toValue: -40,
			duration: 1000
		 }).start()
		 
		AsyncStorage.getItem('user_id').then((value)  => this.setState({ 'user_id': value }))

		this.onLoadAjaxDefault();
	}

	onLoadAjaxDefault = () => {

		this.setState({ isLoading: true });

		let api_url = (Config.base_url() + "/api/data/dashboard");

		fetch( api_url, {
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then( (response) => response.json() )
		.then( (res) => {

			this.setState({ isLoading: false });

			if ( res.status )
			{
				this.setState({
				  dataSource: res.data
				});
			}

		})
		.catch(error => {console.log(error); alert(error); this.setState({ isLoading: false }); })
		.done();
		this.setState({ isLoading: false });
	}
	
	render()
	{
		const { navigate } = this.props.navigation;
		
		const animatedQuickQuoteStyle 		= { marginRight: this.state.animatedQuickQuotePosition }
		const animatedProductLineStyle 		= { marginRight: this.state.animatedProductlinePosition }
		const animatedPromotionStyle 		= { marginTop: this.state.animatedPromotionPosition }
		const animatedEventStyle 			= { marginLeft: this.state.animatedEventsPosition }
		const animatedQuickSupportStyle 	= { marginTop: this.state.animatedQuickSupportPosition }

		let dataSource = this.state.dataSource;
		
		return(
			<ImageBackground source={require('./../img/background-dashboard.png')} style={{ flex: 1 }} >

				<Spinner visible={this.state.isLoading} textStyle={{color: '#FFF' }} />

				<View style={[styles.bodyContainerDashboard,{flex:1}]}>
					<Animated.View style={[styles.logoContainer, animatedQuickQuoteStyle]} >
						<TouchableOpacity onPress={ () => navigate('Request for Quotation') } style={styles.logoContainer}> 
							<Image source={require('./../img/dashboard/rfq-icon.png')} style={{ width:150, height:130 }} />
						</TouchableOpacity>
					</Animated.View>

					<Animated.View style={[styles.logoContainer2, animatedPromotionStyle]}>
						<TouchableOpacity  onPress={ () => navigate('Newsroom') } >
							<Image source={require('./../img/dashboard/newsroom-icon.png')} style={{ width:115, height:95 }} />
						</TouchableOpacity>
					</Animated.View>

					<Animated.View style={[styles.logoContainer3, animatedEventStyle]}>
						<TouchableOpacity onPress={ () => navigate('Events') }>
							<Image source={require('./../img/dashboard/events-icon.png')} style={{ width:120, height:100 }} />
						</TouchableOpacity>
					</Animated.View>

					<Animated.View style={[styles.logoContainer4, animatedProductLineStyle]}>
						<TouchableOpacity  onPress={ () => navigate('Product Lines') } >
							<Image source={require('./../img/dashboard/product-lines-icon.png')} style={{ width:130, height:110 }} />
						</TouchableOpacity>
					</Animated.View>

					<Animated.View style={[styles.logoContainer5, animatedQuickSupportStyle]}>
						<TouchableOpacity onPress={ () => navigate('Support Hub') }>
							<Image source={require('./../img/dashboard/support-hub-icon.png')} style={{ width:145, height:125 }} />
						</TouchableOpacity>
					</Animated.View>
				</View>

				<Footer style={{ backgroundColor:'transparent'}}>
					<View style={styles.bodyContainer}>
						<View style={{ flexDirection:'row' }}>
							<TouchableOpacity  style={{ marginHorizontal:5 }} onPress={() => Linking.openURL( (dataSource && dataSource.socialplugins_facebook) ? dataSource.socialplugins_facebook : '' )}>
								<View style={ styles.normalOpacity5 }>
									<Image source={require('./../img/dashboard/fb-icon.png')} style={{ width:40, height:40 }} />
								</View>
							</TouchableOpacity>
							
							<TouchableOpacity  style={{ marginHorizontal:5 }} onPress={() => Linking.openURL( (dataSource && dataSource.socialplugins_twiiter) ? dataSource.socialplugins_twiiter : '' )}>
								<Image source={require('./../img/dashboard/twitter-icon.png')} style={{ width:40, height:40 }} />
							</TouchableOpacity>

							<TouchableOpacity  style={{ marginHorizontal:5 }}  onPress={() => Linking.openURL( (dataSource && dataSource.socialplugins_linkedin) ? dataSource.socialplugins_linkedin : '' )}>
								<Image source={require('./../img/dashboard/linkedin-icon.png')} style={{ width:40, height:40 }}  />
							</TouchableOpacity>
						</View>
					</View>
				</Footer>

				<Footer style={{ backgroundColor:'transparent' }}>
					<FooterCopyRight />
				</Footer>

			</ImageBackground>
		);	
	}
}